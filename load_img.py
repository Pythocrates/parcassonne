from pathlib import Path

import pygame


IPATH = Path(__file__).parent / 'data' / 'images'
CPATH = IPATH / 'cards'


_name_to_img_dict = dict()

__rot = pygame.transform.rotate

for card_path in CPATH.iterdir():
    name = card_path.stem
    upright = pygame.image.load(card_path.as_posix())
    _name_to_img_dict[name] = [__rot(upright, r) for r in range(0, 360, 90)]

for image_path in IPATH.iterdir():
    if image_path.suffix:
        name = f'i_{image_path.stem}'
        _name_to_img_dict[name] = pygame.image.load(image_path.as_posix())


def str_to_img(name):
    retrieved = _name_to_img_dict[name]
    if not name.startswith('i_'):
        retrieved = retrieved[0]

    return retrieved


def get_tile_image(name, rotate=0):
    retrieved = _name_to_img_dict[name]

    if not name.startswith('i_'):
        retrieved = retrieved[(rotate // 90) % 4]

    return retrieved
