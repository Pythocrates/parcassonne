'''This module contains the Card class.'''

import pygame

import load_img
from enums import CardStatusEnum
from card_tile_enum import CardTileEnum
from direction_enum import DirectionEnum
from base_sprite import BaseSprite


class Card(BaseSprite):
    groups = pygame.sprite.Group()

    _ROTATION_MATRICES = {
        0: ((1, 0), (0, 1)),
        90: ((0, -1), (1, 0)),
        180: ((-1, 0), (0, -1)),
        270: ((0, 1), (-1, 0))
    }

    # the following static values are initialized in "pre_init" static method
    width, height = None, None  # in pixel, e.g. 100 x 100
    subtile_width, subtile_height = None, None  # in subtiles, e.g. 5 x 5
    border_direction_coords = {}  # {"north": (2, 0), "east": (4, 2), ... }
    border_corner_coords = {}  # {"north-east": (4, 0), "south-east": (4, 4),}

    @staticmethod
    def pre_init(width, height, subtile_width, subtile_height):
        Card.width = width
        Card.height = height

        Card.subtile_width = subtile_width
        Card.subtile_height = subtile_height

        border_direction_coords = {}
        border_direction_coords[DirectionEnum.NORTH] = Card.subtile_width//2, 0
        border_direction_coords[DirectionEnum.EAST] = (
            Card.subtile_width - 1, Card.subtile_height // 2)
        border_direction_coords[DirectionEnum.SOUTH] = (
            Card.subtile_width // 2, Card.subtile_height - 1)
        border_direction_coords[DirectionEnum.WEST] = (
            0, Card.subtile_height // 2)
        Card.border_direction_coords = border_direction_coords

        border_corner_coords = {}
        border_corner_coords[DirectionEnum.NORTHEAST] = Card.subtile_width-1, 0
        border_corner_coords[DirectionEnum.SOUTHEAST] = (
            Card.subtile_width - 1, Card.subtile_height - 1)
        border_corner_coords[DirectionEnum.SOUTHWEST] = (
            0, Card.subtile_height - 1)
        border_corner_coords[DirectionEnum.NORTHWEST] = 0, 0
        Card.border_corner_coords = border_corner_coords

    @staticmethod
    def is_border_coord(xxx_todo_changeme):
        """
        Subtile coordinate belongs to the border of the card?
        """
        x, y = xxx_todo_changeme
        return (
            x == 0 or
            x == Card.subtile_width - 1 or
            y == 0 or
            y == Card.subtile_height - 1
        )

    @staticmethod
    def is_on_border_line_coord(xxx_todo_changeme1, direction):
        """
        Subtile coordinate belongs to the border of the card given by a
        direction?
        """
        x, y = xxx_todo_changeme1
        result = False
        if direction == DirectionEnum.NORTH:
            result = (y == 0)
        elif direction == DirectionEnum.EAST:
            result = (x == Card.subtile_width-1)
        elif direction == DirectionEnum.SOUTH:
            result = (y == Card.subtile_height-1)
        elif direction == DirectionEnum.WEST:
            result = (x == 0)
        return result

    def __init__(self, card_deck, filePath, name, crest=None, tiles=None):
        self.card_deck = card_deck  # parent

        self.name = name
        self.file_name = name + ".png"
        self.subtiles = tiles
        self.crest = crest
        if tiles is not None:
            self.rotation_range = self.get_rotation_range()

        img_sprite = load_img.str_to_img(self.name)

        super().__init__(x=0, y=0, image=img_sprite, add_groups=False)

        self.status = CardStatusEnum.IN_DECK

    def __str__(self):
        return '{0}: name = {1}, crest = {2}'.format(
            self.__class__.__name__, self.name, self.crest)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if status == CardStatusEnum.IN_DECK:
            self.remove_sprite()
        elif status == CardStatusEnum.GRABBED:
            self.add_sprite()
        elif status == CardStatusEnum.BACK_DECK:
            pass
        elif status == CardStatusEnum.IN_TILE:
            self.remove_sprite()
        elif status == CardStatusEnum.TRASH:
            self.remove_sprite()
        else:
            raise ValueError

        self._status = status
        self.__status_frames__ = self.card_deck.total_frames

    def update(self, seconds):
        # no need for seconds but the other sprites need it
        if self.status == CardStatusEnum.GRABBED:
            self.rect.center = pygame.mouse.get_pos()

    @staticmethod
    def get_rotated_subtile_xy(xxx_todo_changeme2, rotation=0):
        """
        Rotates a subtile coordinate.
        """
        x, y = xxx_todo_changeme2
        rotation %= 360
        if rotation == 0:
            return x, y

        rm = Card._ROTATION_MATRICES.get(rotation)
        xc = Card.subtile_width // 2
        yc = Card.subtile_height // 2

        xr = rm[0][0] * (x - xc) + rm[0][1] * (y - yc) + xc
        yr = rm[1][0] * (x - xc) + rm[1][1] * (y - yc) + yc
        return xr, yr

    def get_subtile(self, xxx_todo_changeme3, rotation=0):
        """
        Returns a rotated subtile.
        """
        x, y = xxx_todo_changeme3
        if (x < 0 or x >= Card.subtile_width) or (
                y < 0 or y >= Card.subtile_height):
            return None
        xr, yr = self.get_rotated_subtile_xy((x, y), rotation)

        return self.subtiles[xr + yr * Card.subtile_width]

    def get_subtile_direction(self, direction, rotation=0):
        """
        Returns border subtile in "direction" of a "rotated" card
        """
        if direction not in (
                DirectionEnum.NORTH, DirectionEnum.EAST, DirectionEnum.SOUTH,
                DirectionEnum.WEST
        ):
            raise ValueError
        subtile = self.get_subtile(
            self.border_direction_coords[direction], rotation)
        return subtile

    def get_subtile_north(self, rotation=0):
        """
        Returns border subtile in north direction of a rotated card
        """
        subtile = self.get_subtile(
            self.border_direction_coords[DirectionEnum.NORTH], rotation)
        return subtile

    def get_subtile_east(self, rotation=0):
        """
        Returns border subtile in east direction of a rotated card
        """
        subtile = self.get_subtile(
            self.border_direction_coords[DirectionEnum.EAST], rotation)
        return subtile

    def get_subtile_south(self, rotation=0):
        """
        Returns border subtile in south direction of a rotated card
        """
        subtile = self.get_subtile(
            self.border_direction_coords[DirectionEnum.SOUTH], rotation)
        return subtile

    def get_subtile_west(self, rotation=0):
        """
        Returns border subtile in west direction of a rotated card
        """
        subtile = self.get_subtile(
            self.border_direction_coords[DirectionEnum.WEST], rotation)
        return subtile

    def get_rotated_subtiles(self, rotation=0):
        """
        Returns all subtiles of a rotated card.
        """
        rotated_subtiles = []
        for i in range(0, len(self.subtiles)):
            rotated_subtiles.append(
                self.get_subtile(
                    (i % Card.subtile_width, i // Card.subtile_width), rotation
                ))
        return rotated_subtiles

    def get_player_subtiles_xy(self, rotation=0):
        """
        Returns coordinate list of all player-subtiles of a rotated card.
        """
        subtiles_xy = []
        for i in range(0, len(self.subtiles)):
            subtile = self.subtiles[i]
            if subtile in (
                    CardTileEnum.CITY_P, CardTileEnum.FIELD_P,
                    CardTileEnum.WAY_P, CardTileEnum.MONASTERY_P
            ):
                x, y = (i % Card.subtile_width, i // Card.subtile_width)
                subtile_x, subtile_y = self.get_rotated_subtile_xy(
                    (x, y), rotation)
                subtiles_xy.append((subtile_x, subtile_y))
        return subtiles_xy

    def has_subtile(self, subtile):
        """
        Does card contain the given subtile?
        """
        return subtile in self.subtiles

    def get_border_subtiles(self, rotation, isSimplified=False):
        """
        Returns all border subtile of a rotated card.
        If isSimplified True, all found subtiles are simplified.
        """
        border_subtiles = []
        for x in range(0, Card.subtile_width):
            for y in range(0, Card.subtile_height):
                if Card.is_border_coord((x, y)):
                    subtile = self.get_subtile((x, y), rotation)
                    if isSimplified:
                        subtile = subtile.simplified
                    border_subtiles.append(subtile)
        return border_subtiles

    def get_rotation_range(self):
        """
        Rotation range of a card is normally (0, 90, 180, 270)
        However some rotation makes identical card to a previous one.
        These surplus rotations are omitted from the returned range.
        """
        rotation_range = []
        border_subtiles_list = []
        for rotation in range(0, 360, 90):
            border_subtiles = self.get_border_subtiles(
                rotation, isSimplified=True)
            if border_subtiles not in border_subtiles_list:
                rotation_range.append(rotation)
            border_subtiles_list.append(border_subtiles)
        return rotation_range
