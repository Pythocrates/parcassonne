'''This module contains the Tile class.'''

import pygame

from enums import TileType
from card_tile_enum import CardTileEnum
from card import Card
from coord import SubtileCoord, SubtileCoordOutOfBondError


class Tile:
    @staticmethod
    def get_surrounding_tile_positions(xxx_todo_changeme, withDiagonal=False):
        """
        Returns all surrounding tile positions of a tile given by its position.
        """
        (xt, yt) = xxx_todo_changeme
        positions = []

        for xd in [-1, 0, 1]:
            for yd in [-1, 0, 1]:
                if abs(xd)+abs(yd) == 1 or (withDiagonal and abs(xd)+abs(yd) == 2):
                    positions.append((xt+xd, yt+yd))

        return positions

    def __init__(self, tile_deck, x, y, width, height, status, card, rotation):
        self.width = width
        self.height = height
        self.rect = pygame.Rect(((0, 0), (self.width - 2, Card.height - 2)))

        self.tile_deck = tile_deck # parent

        self.xt, self.yt = x, y # measured in tiles, not in pixels
        self.card = card
        self.rotation = rotation # (0, 90, 180, 270)

        self.subtiles = [] # same as self.card.subtiles but it is already rotated
        self.player_subtiles = {} # { (x,y) : miniPlayer | None, ... }, where x,y in subtile position, it is already rotated

        self.markedSubtiles = [] # contains dictionaries { "isAreaClosed" : True | False, "subtiles" : [ (x,y), ...] }, where x,y in subtile position, it is already rotated, ...

        self.status = status

    def __eq__(self, othr):
        return id(self) == id(othr)

    def __ne__(self, othr):
        return not self.__eq__(othr)

    def __hash__(self):
        return hash(id(self))

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if status == TileType.ALMOST_TILE:
            self.update_subtiles()
        elif status == TileType.PSEUDO_TILE:
            self.update_subtiles()
        elif status == TileType.FIXED_TILE:
            self.update_subtiles()
        else:
            raise ValueError

        self._status = status
        self.__status_frames__ = self.tile_deck.total_frames

    def get_xy(self):
        """
        Returns the position of a tile.
        """
        return (self.xt, self.yt)

    def get_subtile(self, xxx_todo_changeme1):
        """
        Returns a subtile (given by its position) of a tile.
        """
        (x, y) = xxx_todo_changeme1
        if (x < 0 or x >= Card.subtile_width) or (y < 0 or y >= Card.subtile_height):
            raise ValueError

        if len(self.subtiles) > 0:
            subtile = self.subtiles[x + y * Card.subtile_width]
        else:
            subtile = self.card.get_subtile((x,y), self.rotation)

        return subtile

    def get_subtile_topleft_xy(self, xxx_todo_changeme2):
        """
        Returns (x,y) pixel position of a subtile inside a tile.
        """
        (xst, yst) = xxx_todo_changeme2
        if (xst < 0 or xst >= Card.subtile_width) or (yst < 0 or yst >= Card.subtile_height):
            return None
        return (xst * (self.width // Card.subtile_width), yst * (self.width // Card.subtile_width))

    def get_subtile_north(self):
        """
        Returns border subtile in north direction of a rotated card.
        """
        if len(self.subtiles) > 0:
            north_pos = (Card.subtile_width//2, 0)
            subtile = self.get_subtile(north_pos)
        else:
            subtile = self.card.get_subtile_north(self.rotation)
        return subtile

    def get_subtile_east(self):
        """
        Returns border subtile in east direction of a rotated card.
        """
        if len(self.subtiles) > 0:
            east_pos = (Card.subtile_width-1, Card.subtile_height//2)
            subtile = self.get_subtile(east_pos)
        else:
            subtile = self.card.get_subtile_east(self.rotation)
        return subtile

    def get_subtile_south(self):
        """
        Returns border subtile in south direction of a rotated card.
        """
        if len(self.subtiles) > 0:
            south_pos = (Card.subtile_width//2, Card.subtile_height-1)
            subtile = self.get_subtile(south_pos)
        else:
            subtile = self.card.get_subtile_south(self.rotation)
        return subtile

    """
    Returns border subtile in west direction of a rotated card.
    """
    def get_subtile_west(self):
        if len(self.subtiles) > 0:
            west_pos = (0, Card.subtile_height//2)
            subtile = self.get_subtile(west_pos)
        else:
            subtile = self.card.get_subtile_west(self.rotation)
        return subtile

    """
    Update subtiles of the tile for example after a rotation.
    """
    def update_subtiles(self):
        self.subtiles = self.card.get_rotated_subtiles(self.rotation)
        self.update_player_subtiles()

    """
    Update player-subtiles of the tile for example after a rotation.
    """
    def update_player_subtiles(self):
        player_subtiles = {}
        for xy in self.card.get_player_subtiles_xy(-1*self.rotation):
            player_subtiles[xy] = None
        self.player_subtiles = player_subtiles

    """
    Search for a player-subtile in a tile and return its position if it is found.
    """
    def get_subtile_xy_by_subtile(self, subtile):
        for xys in list(self.player_subtiles.keys()):
            foundSubtile = self.get_subtile(xys)
            if foundSubtile == subtile:
                return xys
        return None

    """
    Search for an subtile inside a tile where miniPlayer can be placed.
    Returns found subtile position or None.
    Rules during search:
        - at "way"-s, diagonal search is not allowed if there is an adjacent way-cross
        - passing accross 2 diagonal ways is not allowed
    If isFieldToCity is True, from a "field" subtile starts the search and all
        adjacent CITY_P in a list is the result.
    """
    def simpleSubtileSearch(self, xxx_todo_changeme3, isFieldToCity = False):
        (x,y) = xxx_todo_changeme3
        foundSubtileCities = [] # isFieldToCity = True
        xys = SubtileCoord((x,y))
        searchedSubtile = self.get_subtile(xys.get_xy())
        if not searchedSubtile.is_passable:
            return None
        searchedSubtileSimplified = searchedSubtile.simplified
        searchedSubtileExtended = searchedSubtile.extended
        if not isFieldToCity:
            if searchedSubtile == searchedSubtileExtended:
                return xys.get_xy()
        else:
            if searchedSubtileSimplified != CardTileEnum.FIELD:
                raise ValueError

        toSearch = set()
        toSearch.add(xys)
        afterSearch = set()

        while not len(toSearch) == 0:
            xys = toSearch.pop()
            if xys in afterSearch:
                continue
            afterSearch.add(xys)

            subtile = self.get_subtile(xys.get_xy())
            if not isFieldToCity:
                if subtile == searchedSubtileExtended:
                    return xys.get_xy()
                elif subtile != searchedSubtileSimplified:
                    continue
            else:
                if subtile == CardTileEnum.CITY_P:
                    if xys.get_xy() not in foundSubtileCities:
                        foundSubtileCities.append(xys.get_xy())
                    continue
                elif subtile.simplified not in (CardTileEnum.CITY, CardTileEnum.FIELD):
                    continue

            for xd in [-1, 0, 1]:
                for yd in [-1, 0, 1]:
                    if abs(xd)+abs(yd) == 1: # north, east, south, west
                        try:
                            toSearch.add(xys.get_add_xy((xd, yd)))
                        except SubtileCoordOutOfBondError:
                            pass
                    elif abs(xd)+abs(yd) == 2: # north-east, south-east, south-west, north-west
                        # Simon modify 20190116
                        if searchedSubtileSimplified == CardTileEnum.WAY: # diagonal movement allowed
                        #if searchedSubtileSimplified in (CardTileEnum.WAY, CardTileEnum.FIELD): # diagonal movement allowed
                            try:
                                diagonal_subtile1 = self.get_subtile((x+xd,y)).simplified
                            except ValueError:
                                diagonal_subtile1 = None
                            try:
                                diagonal_subtile2 = self.get_subtile((x,y+yd)).simplified
                            except ValueError:
                                diagonal_subtile2 = None
                            if searchedSubtileSimplified == CardTileEnum.WAY and (diagonal_subtile1 == CardTileEnum.WAY_CROSS or diagonal_subtile2 == CardTileEnum.WAY_CROSS):
                                pass
                            elif searchedSubtileSimplified in [CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.MONASTERY] and (diagonal_subtile1 in [CardTileEnum.WAY, CardTileEnum.WAY_CROSS] and diagonal_subtile2 in [CardTileEnum.WAY, CardTileEnum.WAY_CROSS]):
                                pass
                            else:
                                try:
                                    toSearch.add(xys.get_add_xy((xd, yd)))
                                except SubtileCoordOutOfBondError:
                                    pass

        if not isFieldToCity:
            return None
        else:
            return foundSubtileCities
