'''This module contains the data of the base carcassonne card deck.'''

from card_tile_enum import CardTileEnum


CARD_DECK_TEMPLATE = {
    'card_width': 100,
    'card_height': 100,
    'subtile_width': 5,
    'subtile_height': 5,
    'start_card_name': 'city_border_w_way',
    'cards': {
        'back': {
            'total': 0
        },
        'marker': {
            'total': 0
        },
        'none': {
            'total': 0
        },
        '2_city_borders_on_edge': {
            'total': 2,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY_P,
                CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.WAY_CROSS
            ]
        },
        '2_city_borders': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY,
                CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY_P,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY
            ]
        },
        '3cross': {
            'total': 4,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.WAY_P, CardTileEnum.WAY, CardTileEnum.WAY_CROSS, CardTileEnum.WAY_CROSS, CardTileEnum.WAY_P,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        '3cross_w_city_border': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                #CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.WAY_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.WAY_P,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY_CROSS, CardTileEnum.WAY, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD_P
            ]
        },
        '4cross': {
            'total': 1,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.WAY_P, CardTileEnum.WAY, CardTileEnum.WAY_CROSS, CardTileEnum.WAY, CardTileEnum.WAY_P,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_border1': {
            'total': 2,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_border2': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_border_w_curve1': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                #CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.WAY,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD_P,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_border_w_curve2': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.WAY, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_border_w_way': {
            'total': 4,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.WAY, CardTileEnum.WAY, CardTileEnum.WAY, CardTileEnum.WAY_P, CardTileEnum.WAY,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_edge': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_edge_w_crest_a_curve': {
            'total': 2,
            'crest': True,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY,
                #CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD_P,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD_P,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_edge_w_crest': {
            'total': 2,
            'crest': True,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_edge_w_curve': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD_P,
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'city_main_w_crest': {
            'total': 1,
            'crest': True,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
            ]
        },
        'city_middle_border': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY
            ]
        },
        'city_middle_border_w_crest': {
            'total': 1,
            'crest': True,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY
            ]
        },
        'city_middle_border_w_way': {
            'total': 1,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.WAY_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.CITY
            ]
        },
        'city_middle': {
            'total': 1,
            'crest': False,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY
            ]
        },
        'city_middle_w_crest': {
            'total': 2,
            'crest': True,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.CITY
            ]
        },
        'city_w_crest_a_way': {
            'total': 2,
            'crest': True,
            'tiles': [
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.CITY, CardTileEnum.WAY_P, CardTileEnum.CITY, CardTileEnum.CITY,
                CardTileEnum.CITY, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.CITY
            ]
        },
        'left_curve1': {
            'total': 6,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.WAY, CardTileEnum.WAY_P, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'left_curve2': {
            'total': 3,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.WAY, CardTileEnum.WAY_P, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'monastery': {
            'total': 4,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.MONASTERY, CardTileEnum.MONASTERY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.MONASTERY, CardTileEnum.MONASTERY_P, CardTileEnum.MONASTERY, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.MONASTERY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'monastery_w_way': {
            'total': 2,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.MONASTERY, CardTileEnum.MONASTERY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.MONASTERY, CardTileEnum.MONASTERY, CardTileEnum.MONASTERY_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'way': {
            'total': 4,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        },
        'way2': {
            'total': 4,
            'crest': False,
            'tiles': [
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD_P, CardTileEnum.WAY, CardTileEnum.FIELD_P, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY_P, CardTileEnum.FIELD, CardTileEnum.FIELD,
                CardTileEnum.FIELD, CardTileEnum.FIELD, CardTileEnum.WAY, CardTileEnum.FIELD, CardTileEnum.FIELD
            ]
        }
    }
}
