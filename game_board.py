'''This module contains the GameBoard class.'''

import pygame

import load_img
from enums import GameBoardEnum, CardStatusEnum, TileType
from card import Card
from card_deck import CardDeck
from card_deck_template import CARD_DECK_TEMPLATE
from tile_deck import TileDeck
from mini_player import MiniPlayer
from player_deck import PlayerDeck
from base_sprite import BaseSprite


class GameBoard:
    board_img = load_img.str_to_img('i_border')
    start_img = load_img.str_to_img('i_start')

    def __init__(self, fps, is_test=False):
        self.total_frames = 0
        self.fps = fps
        self._is_test = is_test

        self._status = GameBoardEnum.DECK
        self.__status_frames__ = self.total_frames
        self.is_auto_focus = True

        self.card_deck = CardDeck(self, CARD_DECK_TEMPLATE)
        self.tile_deck = TileDeck(
            self, offset=(219, 31), size=(551, 538), grid_size=(6, 6))
        self.player_deck = PlayerDeck(self)

        self.grabbed_object = None  # e.g. card, tile, miniPlayer

        self.buttons = {}

        if CardDeck.startCardName:
            card = self.card_deck.pick()
            self.tile_deck.add_tile(
                0, 0, card, 0, TileType.FIXED_TILE, is_initial=True)

    @property
    def is_test(self):
        return self._is_test

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        status_old = self.status
        status_new = None  # status modified inside this function

        if (
                status_old is GameBoardEnum.CARD_ALMOST_TILE and
                status is not GameBoardEnum.CARD_ALMOST_TILE
        ):
            self.tile_deck.deleteButtons()

        if status is GameBoardEnum.DECK:
            if status_old in [
                    GameBoardEnum.CARD_IN_TILE, GameBoardEnum.FIGURE_GRABBED]:
                if True:  # for test purpose of end game
                    # Simon modify 20190116
                    # self.player_deck.step start at 0
                    if self.player_deck.step >= 64:
                        # set is_computer to False and effect at next turn
                        # self.player_deck.current_player.is_computer = False
                        pass
                    # End Simon modify 20190116
                self.player_deck.step += 1
                if not self.getPickedCard(isConst=True):
                    status_new = GameBoardEnum.END
        elif status == GameBoardEnum.END:
            self.analyseEndBoard()
        elif status in [
                GameBoardEnum.CARD_GRABBED,
                GameBoardEnum.BACK_CARD,
                GameBoardEnum.CARD_ALMOST_TILE,
                GameBoardEnum.CARD_IN_TILE,
                GameBoardEnum.FIGURE_GRABBED,
                GameBoardEnum.NEW_GAME,
        ]:
            pass
        else:
            raise ValueError()

        self._status = status
        self.__status_frames__ = self.total_frames

        if status_new:
            self.status = status_new

    def add_player(self, name, color, is_computer):
        """
        Add a new player to the player_deck
        """
        self.player_deck.add_player(name, color, is_computer)

    def drawButtons(self):
        """
        Draw (display) buttons (rotation, ok, cancel) around a deposited almost
        card.
        """
        for name, button in self.buttons.items():
            button.draw()

    def processButtons(self):
        """
        Call button action of a clicked button.
        """
        mouse_pos = pygame.mouse.get_pos()
        for button in self.buttons.values():
            if button.collidepoint(mouse_pos):
                button.action()
                break

    def draw(self, step=None):
        """
        Draw the complete gameBoard.
        """
        # Simon modify 20190120
        if step is None:
            step = self.player_deck.step
        # End Simon 20190120

        screen = pygame.display.get_surface()

        self.tile_deck.draw()
        screen.blit(self.board_img, (0, 0))

        self.card_deck.draw()

        self.player_deck.draw(step)

        self.drawButtons()

        Card.groups.update(1)
        MiniPlayer.groups.update(1)

        BaseSprite.groups.draw(screen)

    def getPickedCard(self, isConst=False):
        picked_cards = []
        card = None
        legal_almost_tiles = {}

        # pull a card from the deck that can be placed legally to the board
        while len(legal_almost_tiles) == 0:
            card = self.card_deck.pick()
            if card is None:
                break
            elif isConst:
                picked_cards.append(card)
            legal_almost_tiles = self.tile_deck.get_legal_almost_tiles(
                card, isOnlyFirst=True)
            if len(legal_almost_tiles) == 0:
                if not isConst:
                    card.status = CardStatusEnum.TRASH

        if isConst:
            for card in reversed(picked_cards):
                self.card_deck.restore(card)

        return card

    def pick_card(self):
        """
        Pick a card from the deck, the card gets grabbed and it can be moved by
        mouse. If computer is on turn, it DOES make a complete turn.
        """
        player = self.player_deck.current_player
        card = self.getPickedCard()

        if card is not None:
            if player.is_computer:
                # computer deposits the picked card immediately
                target_tile, mini_player_xyts = (
                    self.tile_deck.getComputerCalculatedStep(card))

                # Simon added 20190120
                if self.is_auto_focus:
                    self.MapMovetoCenter(target_tile)
                # End Simon added 20190120

                self.depositCard(target_tile, mini_player_xyts)
            else:
                card.status = CardStatusEnum.GRABBED
                self.grabbed_object = card
                self.status = GameBoardEnum.CARD_GRABBED
        else:
            # we have run out of the card stack
            self.status = GameBoardEnum.END

    def MapMovetoCenter(self, targetTile):
        """
        Move tile map to center of target
        """
        xt, yt = targetTile.get_xy()
        display_x, display_y = self.tile_deck.center_x, self.tile_deck.center_y
        while display_x != xt or display_y != yt:
            display_x, display_y = (
                self.tile_deck.center_x, self.tile_deck.center_y)
            if display_x < xt:
                self.tile_deck.center_x += 1
            elif display_x > xt:
                self.tile_deck.center_x -= 1

            if display_y < yt:
                self.tile_deck.center_y += 1
            elif display_y > yt:
                self.tile_deck.center_y -= 1

            self.draw()

            pygame.display.flip()

    def restore_card(self):
        """
        Restores a picked card into the deck.
        """
        card = self.grabbed_object
        if card is None:
            return
        self.card_deck.restore(card)
        card.status = CardStatusEnum.IN_DECK
        self.status = GameBoardEnum.DECK

    def depositCard(self, targetTile=None, miniPlayer_xyts=None):
        """
        Deposit a picked card into the board.
        If computer is on turn (targeTile != None), the card is placed based
        on the targetTile tile. If miniPlayer_xyts is set, computer also
        deposits a miniPlayer.
        """
        if targetTile is None:
            card = self.grabbed_object
        else:
            card = targetTile.card
        if card is None:
            return
        addedTile = self.tile_deck.depositCard(card, targetTile)
        if addedTile is not None:
            if addedTile.status == TileType.FIXED_TILE:
                self.status = GameBoardEnum.CARD_IN_TILE
                # computer player
                if miniPlayer_xyts is not None:
                    # computer deposits miniPlayer immediately
                    self.depositMiniPlayer(miniPlayer_xyts)
                else:
                    self.tile_deck.analyseBoardByTile(addedTile)
                    self.status = GameBoardEnum.DECK
            elif addedTile.status == TileType.ALMOST_TILE:
                # human player
                self.status = GameBoardEnum.CARD_ALMOST_TILE
        else:
            self.restore_card()

    def grabMiniPlayer(self):
        """
        Human player grabs a miniPlayer to be deposited if there is a free one.
        """
        player = self.player_deck.current_player
        miniPlayer = player.pickMiniPlayer()
        if miniPlayer is None:
            self.tile_deck.analyseBoardByTile(player.last_tile)
            self.status = GameBoardEnum.DECK
        else:
            self.status = GameBoardEnum.FIGURE_GRABBED

    def depositMiniPlayer(self, miniPlayer_xyts=None):
        """
        Human player deposits an earlier grabbed miniPlayer to a subtile.
        """
        isDeposited = self.tile_deck.depositMiniPlayer(miniPlayer_xyts)
        if isDeposited is None:
            return
        elif not isDeposited:
            self.restoreMiniPlayer(miniPlayer_xyts)
        self.status = GameBoardEnum.DECK

    def restoreMiniPlayer(self, miniPlayer_xyts=None):
        """
        Earlier grabbed miniplayer by human player is restored to its initial
        place.
        """
        player = self.player_deck.current_player
        if miniPlayer_xyts is None:
            tile = player.last_tile
        else:
            tile = self.tile_deck.get_tile_by_xy(miniPlayer_xyts.get_xyt())
        self.tile_deck.analyseBoardByTile(tile)
        player.restoreMiniPlayer()
        self.status = GameBoardEnum.DECK

    def analyseEndBoard(self):
        """
        Analyse end board. Add final scores to players.
        """
        self.tile_deck.analyseEndBoard(isPseudo=False)

    def create_test_tiles(self):
        """
        Deposits some interesting cards. Only for test purpose, otherwise it is
        not used.
        """
        card_name_problems = (
            'city_edge_w_crest_a_curve', 'city_border_w_curve2',
            'city_border_w_curve1', '2_city_borders_on_edge',
            '3cross_w_city_border', 'city_edge_w_curve')
        card_names = set()
        card = self.card_deck.pick()
        x = 0
        while card is not None:
            if card.name not in card_names and card.name in card_name_problems:
                self.tile_deck.add_tile(x, 0, card, 0, TileType.FIXED_TILE)
                x += 2
                card_names.add(card.name)
            card = self.card_deck.pick()
