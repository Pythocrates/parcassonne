'''This module contains the CardDeck class.'''

from pathlib import Path
import random

import pygame

import load_img
from card import Card
from card_deck_template import CARD_DECK_TEMPLATE
from direction_enum import DirectionEnum
from enums import GameBoardEnum
from player import Player


class CardDeck(pygame.Rect):
    cardBack = None
    cardMarker = None
    cardNone = None

    startCardName = None  # 'city_border_w_way' - filled from template

    cardFilePath = Path(__file__).parent / 'data' / 'images' / 'cards'

    stack_topleft = (35, 415)
    stack_img = load_img.str_to_img('stack')

    font_size = 40
    font_color = (255, 255, 0)  # yellow
    font_type = 'arial'

    def __init__(self, game_board, template):
        self.game_board = game_board
        self.template = template
        self.cards = []
        self.unique_cards = {}  # {card_name: card, ...}
        self.card_pair_cache = {}  # {card_pair_id: {xys1: xys2, ...}, ...}

        if template == CARD_DECK_TEMPLATE:
            rect = self.stack_img.get_rect()
            super().__init__(self.stack_topleft, (rect.width, rect.height))

            self.__class__.startCardName = template['start_card_name']

            Card.pre_init(
                template['card_width'], template['card_height'],
                template['subtile_width'], template['subtile_height'])
            template_cards = template['cards']
            for key in template_cards:
                value = template_cards[key]
                if key == 'back' and value['total'] == 0:
                    CardDeck.cardBack = Card(
                        self, CardDeck.cardFilePath, key, None, None)
                elif key == 'marker' and value['total'] == 0:
                    CardDeck.cardMarker = Card(
                        self, CardDeck.cardFilePath, key, None, None)
                elif key == 'none' and value['total'] == 0:
                    CardDeck.cardNone = Card(
                        self, CardDeck.cardFilePath, key, None, None)
                for i in range(0, value['total']):
                    card = Card(
                        self, CardDeck.cardFilePath, key, value['crest'],
                        value['tiles'])
                    self.cards.append(card)
                    if i == 0:
                        self.unique_cards[card.name] = card

        if not self.is_test:
            random.shuffle(self.cards)

            # place start card to the end of the list
            if CardDeck.startCardName is not None:
                for card in self.cards:
                    if card.name == CardDeck.startCardName:
                        self.cards.remove(card)
                        self.cards.append(card)
                        break

    @property
    def is_test(self):
        return self.game_board.is_test

    @property
    def total_frames(self):
        return self.game_board.total_frames

    def __del__(self):
        del self.game_board
        self.template.clear()
        del self.template
        del self.cards[:len(self.cards)]
        del self.cards
        self.unique_cards.clear()
        del self.unique_cards
        self.card_pair_cache.clear()
        del self.card_pair_cache
        del self

    def shuffle(self):
        """
        Shuffle carddeck
        """
        random.shuffle(self.cards)

    def is_empty(self):
        """
        Is card deck empty?
        """
        return len(self.cards) == 0

    def pick(self):
        """
        Pick one card from the deck
        """
        if len(self.cards) > 0:
            card = self.cards.pop()
        else:
            card = None
        return card

    def restore(self, card):
        """
        Restore given card into the deck
        """
        self.cards.append(card)

    def fill_card_pair_cache(self):
        """
        Fills matching card pairs in all combinations.
        Returns a dictionary: { card_pair_id : {} }
        where card_pair_id is for example
        city_middle_border_w_crest-270-w-monastery-90
        Using CARD_DECK_TEMPLATE there are 12816 pairs.
        """
        card_pair_cache = {}
        for card0 in self.unique_cards.values():
            for rotation0 in (0, 90, 180, 270):
                for direction in (
                        DirectionEnum.NORTH, DirectionEnum.EAST,
                        DirectionEnum.SOUTH, DirectionEnum.WEST
                ):
                    for card1 in self.unique_cards.values():
                        if card0.name == card1.name:
                            continue

                        for rotation1 in (0, 90, 180, 270):
                            if card0.get_subtile_direction(direction, rotation0) == card1.get_subtile_direction(direction.opposite, rotation1):
                                id = card0.name+'-'+str(rotation0)+'-'+direction.value+'-'+card1.name+'-'+str(rotation1)
                                card_pair_cache[id] = {}
        self.card_pair_cache = card_pair_cache

    def draw(self):
        """
        Display deck
        """
        screen = pygame.display.get_surface()
        if not self.is_empty():
            screen.blit(CardDeck.stack_img, self.topleft)
            screen.blit(
                Player.write(
                    str(len(self.cards)), CardDeck.font_size,
                    CardDeck.font_color
                ), (
                    self.left + (self.width - CardDeck.font_size) // 2,
                    self.top + (self.height - CardDeck.font_size) // 2
                )
            )

        if self.game_board.status is GameBoardEnum.END:
            screen.blit(self.game_board.start_img, self.stack_topleft)
