'''This module contains the Player class.'''

import pygame

import load_img
from enums import PlayerStatusEnum, MiniPlayerStatusEnum, TileType
from mini_player import MiniPlayer


class Player:
    max_mini_players = 7
    color_rgb = {
        'black': (0, 0, 0), 'blue': (0, 0, 255), 'green': (0, 255, 0),
        'red': (255, 0, 0), 'yellow': (255, 255, 0)
    }

    def __init__(self, player_deck, name, color, is_computer):
        self.player_deck = player_deck

        self.status = PlayerStatusEnum.IN_BASE

        self.name = name
        self.color = color

        self.is_computer = is_computer
        self.score = 0
        self.pseudoScore = 0
        # Simon added 20190126
        self.addScore = 0
        self.endScore = 0
        # End Simon 20190126

        # containing max 2 FIXED_TILE elements (last-but-one and last) and
        # additional optional PSEDUDO_TILE elements
        self.last_tiles = []

        self.set_color(color)

        #self.border_rect = pygame.Rect(((x, y), (self.border_width, self.image.get_rect().height)))
        border_xy = self.border_topleft[0], self.border_topleft[1] + len(player_deck.players) * int(self.image.get_rect().height * 1.3 + self.font_add_score_size)
        self.border_rect = pygame.Rect((border_xy), (self.border_width, self.image.get_rect().height))

        self.miniPlayers = [] #Player.max_mini_players
        for i in range(0, Player.max_mini_players):
            self.miniPlayers.append(MiniPlayer(self))

    def __del__(self):
        del self.player_deck

    def __str__(self):
        return (
            f'{self.__class__.__name__}: name = {self.name}, '
            f'color = {self.color}, is_computer = {self.is_computer}, '
            f'score = {self.score}, miniPlayers = {self.miniPlayers}'
        )

    def __eq__(self, othr):
        return id(self) == id(othr)

    def __ne__(self, othr):
        return not self.__eq__(othr)

    def __hash__(self):
        return hash(id(self))

    @property
    def status(self):
        return self._status

    @property
    def total_frames(self):
        return self.player_deck.total_frames

    @property
    def border_width(self):
        return self.player_deck.border_width

    @property
    def border_topleft(self):
        return self.player_deck.border_topleft

    @property
    def font_add_score_size(self):
        return self.player_deck.font_add_score_size

    @property
    def game_board(self):
        return self.player_deck.game_board

    @status.setter
    def status(self, status):
        if status == PlayerStatusEnum.IN_BASE:
            pass
        else:
            raise ValueError

        self._status = status
        self.__status_frames__ = self.total_frames

    # Simon added 20190129
    @staticmethod
    def write(msg="pygame is cool", size = 10, color = (0,0,0)):
        myfont = pygame.font.Font("data/Anonymous_Pro.ttf", size)
        mytext = myfont.render(msg, True, color)
        mytext = mytext.convert_alpha()
        return mytext
    # End Simon 20190129

    """
    Returns score of the player
    """
    def get_score(self, isPseudo=False):
        return self.pseudoScore if isPseudo else self.score

    """
    Set score of the player
    """
    def set_score(self, score, isPseudo=False):
        if not isPseudo:
            self.score = score
        else:
            self.pseudoScore = score

    """
    Add score to the player
    """
    def add_score(self, score, isPseudo=False, isEndGame = False, miniPlayer = None):
        if not isPseudo:
            self.score += score
            if isEndGame and score > 0: # miniPlayer != None
                self.endScore += score
                miniPlayer.eScore = score
            elif score > 0:
                self.addScore = score # Simon added 20190126
        else:
            self.pseudoScore += score

    """
    Set color and proper images of the player
    """
    def set_color(self, color):
        self.color = color
        #image_original = pygame.image.load(self.imgFilePath + 'pawn_{0}.png'.format(color))
        image_original = load_img.str_to_img('i_pawn_{0}'.format(color))
        self.image = pygame.transform.scale(image_original, (32, 32))
        self.image_subtile = pygame.transform.scale(image_original, (20, 20))
        self.image_player_deck = pygame.transform.scale(image_original, (10, 10))

    """
    Return miniPlayer list with the given status
    """
    def getMiniPlayersByType(self, status):
        return [miniPlayer for miniPlayer in self.miniPlayers if miniPlayer.status == status]

    """
    Pick a miniPlayer, if available, from the base board
    """
    def pickMiniPlayer(self):
        miniPlayersInBase = self.getMiniPlayersByType(MiniPlayerStatusEnum.IN_BASE)
        if len(miniPlayersInBase) > 0:
            miniPlayer = miniPlayersInBase[0]
            if not self.is_computer:
                miniPlayer.status = MiniPlayerStatusEnum.GRABBED
                self.game_board.grabbed_object = miniPlayer
        else:
            miniPlayer = None
        return miniPlayer

    """
    Restore earlier picked miniPlayer back to the base board
    """
    def restoreMiniPlayer(self):
        miniPlayer = self.game_board.grabbed_object
        miniPlayer.status = MiniPlayerStatusEnum.IN_BASE

    @property
    def last_tile(self):
        """
        Returns last element from self.last_tiles
        """
        if self.last_tiles:
            return self.last_tiles[-1]

    def pop_last_tile(self):
        """
        Removes and returns last element from self.last_tiles
        """
        try:
            last_tile = self.last_tiles.pop()
        except IndexError:
            last_tile = None

        return last_tile

    def append_last_tiles(self, tile):
        """
        Add a tile to self.last_tiles, where list
        contains max 2 FIXED_TILE elements (last-but-one and last)
        and any optional PSEDUDO_TILE elements
        """
        if tile.status == TileType.PSEUDO_TILE:
            self.last_tiles.append(tile)
        elif tile.status not in (TileType.ALMOST_TILE, TileType.FIXED_TILE):
            raise ValueError

        if len(self.last_tiles) == 0:
            self.last_tiles.append(tile)
        elif len(self.last_tiles) == 1 and self.last_tiles[0].status in (TileType.ALMOST_TILE, TileType.FIXED_TILE):
            self.last_tiles.append(tile)
        elif len(self.last_tiles) >= 1 and self.last_tiles[0].status == TileType.PSEUDO_TILE:
            self.last_tiles.insert(0, tile)
        elif len(self.last_tiles) >= 2 and self.last_tiles[1].status in (TileType.ALMOST_TILE, TileType.FIXED_TILE):
            self.last_tiles.pop(0)
            self.last_tiles.insert(1, tile)
        elif len(self.last_tiles) >= 2 and self.last_tiles[1].status == TileType.PSEUDO_TILE:
            self.last_tiles.insert(1, tile)

    def isValid(self):
        return (self.name != None and self.name) and (self.color != None and self.color) and self.is_computer != None
