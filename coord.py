'''This module contains the coordinate classes.'''

import unittest


class SubtileCoordOutOfBondError(Exception):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __str__(self):
        return repr(self.value)


class TileCoord():
    def __init__(self, xxx_todo_changeme1):
        self.x, self.y = xxx_todo_changeme1

    def __del__(self):
        del self.x
        del self.y

    def __eq__(self, other):
        return (
            isinstance(other, type(self)) and self.get_xy() == other.get_xy())

    def __ne__(self, othr):
        return not self.__eq__(othr)

    def __hash__(self):
        return hash(self.get_xy())

    def __str__(self):
        return "class={0}: (x,y)={1}".format(
            self.__class__.__name__, self.get_xy())

    def add(self, tile_coord):
        self.add_xy(tile_coord.get_xy())

    def add_x(self, x):
        self.x += x

    def add_y(self, y):
        self.y += y

    def add_xy(self, xxx_todo_changeme2):
        (x, y) = xxx_todo_changeme2
        self.add_x(x)
        self.add_y(y)

    def get_add(self, otherCoord):
        tileCoord = TileCoord(self.get_xy())
        tileCoord.add(otherCoord)
        return tileCoord

    def get_added_x(self, x):
        tileCoord = TileCoord(self.get_xy())
        tileCoord.add_x(x)
        return tileCoord

    def get_added_y(self, y):
        tileCoord = TileCoord(self.get_xy())
        tileCoord.add_y(y)
        return tileCoord

    def get_added_xy(self, xxx_todo_changeme3):
        (x, y) = xxx_todo_changeme3
        tileCoord = TileCoord(self.get_xy())
        tileCoord.add_xy((x, y))
        return tileCoord

    def get_xy(self):
        return (self.x, self.y)


class SubtileCoord():
    subtile_width = 5
    subtile_height = 5

    @staticmethod
    def is_legal_x(x):
        return x >= 0 and x < SubtileCoord.subtile_width

    @staticmethod
    def is_legal_y(y):
        return y >= 0 and y < SubtileCoord.subtile_height

    @staticmethod
    def is_legal(xxx_todo_changeme):
        (x, y) = xxx_todo_changeme
        x, y = (x, y)
        return SubtileCoord.is_legal_x(x) and SubtileCoord.is_legal_y(y)

    def __init__(self, xxx_todo_changeme4):
        # or embedded ValueError can be used
        x, y = xxx_todo_changeme4
        if not SubtileCoord.is_legal((x, y)):
            raise SubtileCoordOutOfBondError((x, y))
        self.x = x
        self.y = y
        self.x_of = 0  # overflow in x
        self.y_of = 0  # overflow in y

    def __del__(self):
        del self.x
        del self.y
        del self.x_of
        del self.y_of

    def __eq__(self, othr):
        return isinstance(othr, type(self)) and self.get_xy() == othr.get_xy()

    def __ne__(self, othr):
        return not self.__eq__(othr)

    def __hash__(self):
        return hash(self.get_xy())

    def __str__(self):
        return "class={0}: (x,y)={1}".format(
            self.__class__.__name__, self.get_xy())

    def add(self, subtileCoord, isOverflowAllowed=False):
        self.add_xy(subtileCoord.get_xy(), isOverflowAllowed)

    def add_x(self, x, isOverflowAllowed=False):
        if isOverflowAllowed:
            x_of = (self.x + x)//SubtileCoord.subtile_width
            self.x += x
            self.x %= SubtileCoord.subtile_width
        else:
            x_of = 0
            if not SubtileCoord.is_legal_x(self.x + x):
                raise SubtileCoordOutOfBondError(x)
            self.x += x
        self.x_of = x_of

    def add_y(self, y, isOverflowAllowed=False):
        if isOverflowAllowed:
            y_of = (self.y + y)//SubtileCoord.subtile_height
            self.y += y
            self.y %= SubtileCoord.subtile_height
        else:
            y_of = 0
            if not SubtileCoord.is_legal_y(self.y + y):
                raise SubtileCoordOutOfBondError(y)
            self.y += y
        self.y_of = y_of

    def add_xy(self, xxx_todo_changeme5, isOverflowAllowed=False):
        (x, y) = xxx_todo_changeme5
        self.add_x(x, isOverflowAllowed)
        self.add_y(y, isOverflowAllowed)

    def get_add(self, otherCoord, isOverflowAllowed=False):
        subtileCoord = SubtileCoord(self.get_xy())
        subtileCoord.add(otherCoord, isOverflowAllowed)
        return subtileCoord

    def get_add_x(self, x, isOverflowAllowed=False):
        subtileCoord = SubtileCoord(self.get_xy())
        subtileCoord.add_x(x, isOverflowAllowed)
        return subtileCoord

    def get_add_y(self, y, isOverflowAllowed=False):
        subtileCoord = SubtileCoord(self.get_xy())
        subtileCoord.add_y(y, isOverflowAllowed)
        return subtileCoord

    def get_add_xy(self, xxx_todo_changeme6, isOverflowAllowed=False):
        x, y = xxx_todo_changeme6
        subtileCoord = SubtileCoord(self.get_xy())
        subtileCoord.add_xy((x, y), isOverflowAllowed)
        return subtileCoord

    def get_xy(self):
        return (self.x, self.y)


class SubtileWorldCoord:
    def __init__(self, xxx_todo_changeme7):
        ((xt, yt), (xs, ys)) = xxx_todo_changeme7
        self.tileCoord = TileCoord((xt, yt))
        self.subtileCoord = SubtileCoord((xs, ys))  # it may raise Exception

    def __del__(self):
        del self.tileCoord
        del self.subtileCoord

    def __eq__(self, othr):
        return isinstance(othr, type(self)) and self.get_xy() == othr.get_xy()

    def __ne__(self, othr):
        return not self.__eq__(othr)

    def __hash__(self):
        return hash(self.get_xy())

    def __str__(self):
        return "class={0}: (xt,yt)={1}, (xs,ys)={2}".format(
            self.__class__.__name__, self.tileCoord.get_xy(),
            self.subtileCoord.get_xy())

    def add(self, subtileWorldCoord):
        self.subtileCoord.add(
            subtileWorldCoord.subtileCoord, isOverflowAllowed=True)
        self.tileCoord.add(subtileWorldCoord.tileCoord)
        self.tileCoord.add_xy((self.subtileCoord.x_of, self.subtileCoord.y_of))

    def add_xt(self, x):
        self.tileCoord.add_x(x)

    def add_yt(self, y):
        self.tileCoord.add_y(y)

    def add_xyt(self, xxx_todo_changeme8):
        (x, y) = xxx_todo_changeme8
        self.add_xt(x)
        self.add_yt(y)

    def add_xs(self, x):
        self.subtileCoord.add_x(x, isOverflowAllowed=True)
        self.tileCoord.add_x(self.subtileCoord.x_of)

    def add_ys(self, y):
        self.subtileCoord.add_y(y, isOverflowAllowed=True)
        self.tileCoord.add_y(self.subtileCoord.y_of)

    def add_xys(self, xxx_todo_changeme9):
        (x, y) = xxx_todo_changeme9
        self.add_xs(x)
        self.add_ys(y)

    def get_add(self, otherCoord):
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add(otherCoord)
        return subtileWorldCoord

    def get_add_xt(self, x):
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add_xt(x)
        return subtileWorldCoord

    def get_add_yt(self, y):
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add_yt(y)
        return subtileWorldCoord

    def get_add_xyt(self, xxx_todo_changeme10):
        (x, y) = xxx_todo_changeme10
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add_xyt((x, y))
        return subtileWorldCoord

    def get_add_xs(self, x):
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add_xs(x)
        return subtileWorldCoord

    def get_add_ys(self, y):
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add_ys(y)
        return subtileWorldCoord

    def get_add_xys(self, xxx_todo_changeme11):
        (x, y) = xxx_todo_changeme11
        subtileWorldCoord = SubtileWorldCoord(self.get_xy())
        subtileWorldCoord.add_xys((x, y))
        return subtileWorldCoord

    def get_xy(self):
        return (self.tileCoord.get_xy(), self.subtileCoord.get_xy())

    def get_xyt(self):
        return self.tileCoord.get_xy()

    def get_xys(self):
        return self.subtileCoord.get_xy()


# Here's our "unit tests".
class IsTileCoordTests(unittest.TestCase):

    def test1(self):
        tileCoord = TileCoord((2, 3))
        self.assertEqual(tileCoord.get_xy(), (2, 3))

    def test2(self):
        tileCoord = TileCoord((2, 3))
        tileCoord2 = TileCoord((4, -2))
        tileCoord.add(tileCoord2)
        self.assertEqual(tileCoord.get_xy(), (6, 1))

    def test3(self):
        tileCoord = TileCoord((2, 3))
        tileCoord2 = TileCoord((4, -2))
        self.assertNotEqual(tileCoord, tileCoord2)

    def test4(self):
        tileCoord = TileCoord((2, 3))
        tileCoord2 = TileCoord((2, 3))
        self.assertEqual(tileCoord, tileCoord2)

    def test5(self):
        self.assertEqual(
            TileCoord((2, 3)).get_added_xy((1, 1)).get_xy(), (3, 4))


class IsSubtileCoordTests(unittest.TestCase):

    def test1(self):
        subtileCoord = SubtileCoord((1, 4))
        self.assertEqual(subtileCoord.get_xy(), (1, 4))

    def test2(self):
        subtileCoord = SubtileCoord((1, 4))
        subtileCoord.add_xy((1, -2))
        self.assertEqual(subtileCoord.get_xy(), (2, 2))

    def test3(self):
        subtileCoord = SubtileCoord((1, 4))
        try:
            subtileCoord.add_xy((1, 2))
        except SubtileCoordOutOfBondError:
            pass
        self.assertEqual(subtileCoord.get_xy(), (2, 4))

    def test4(self):
        subtileCoord = SubtileCoord((1, 4))
        try:
            subtileCoord.add_xy((1, 2), isOverflowAllowed=True)
        except SubtileCoordOutOfBondError:
            pass
        self.assertEqual(subtileCoord.get_xy(), (2, 1))
        self.assertEqual(subtileCoord.x_of, 0)
        self.assertEqual(subtileCoord.y_of, 1)

    def test5(self):
        subtileCoord = SubtileCoord((2, 2))
        subtileCoord2 = SubtileCoord((2, 2))
        self.assertEqual(subtileCoord.get_add(subtileCoord2).get_xy(), (4, 4))


class IsSubtileWorldCoordTests(unittest.TestCase):

    def test1(self):
        subtileWorldCoord = SubtileWorldCoord(((1, 1), (2, 2)))
        self.assertEqual(subtileWorldCoord.get_xy(), ((1, 1), (2, 2)))

    def test2(self):
        subtileWorldCoord = SubtileWorldCoord(((1, 1), (2, 2)))
        subtileWorldCoord.add_xys((-2, -2))
        self.assertEqual(subtileWorldCoord.get_xy(), ((1, 1), (0, 0)))

    def test3(self):
        subtileWorldCoord = SubtileWorldCoord(((1, 1), (2, 2)))
        subtileWorldCoord.add_xys((-2, -2))
        subtileWorldCoord.add_xys((-2, -2))
        self.assertEqual(subtileWorldCoord.get_xy(), ((0, 0), (3, 3)))

    def test4(self):
        subtileWorldCoord = SubtileWorldCoord(((0, 0), (3, 3)))
        subtileWorldCoord2 = SubtileWorldCoord(((2, 1), (1, 3)))
        self.assertEqual(
            subtileWorldCoord.get_add(subtileWorldCoord2).get_xy(),
            ((2, 2), (4, 1)))
