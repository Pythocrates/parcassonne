#!/usr/bin/env python

'''
Documentation, License etc.

@package pycassonne2
'''

import time

import pygame

from interaction import interaction
from game_board import GameBoard
from pgu import gui
from player_dialog import PlayerDialog
from enums import GameBoardEnum


AUTO_START = False
FPS = 24
SIZE = 1600, 1200


def main():
    pygame.init()
    pygame.display.set_caption('Parcassonne')
    pygame.display.set_mode(SIZE)

    game_board = GameBoard(FPS, is_test=False)
    game_board.add_player("Papi", "green", False)
    game_board.add_player("Mars", "yellow", True)
    # gameBoard.add_player("Venus", "black", True)
    # gameBoard.add_player("Bazsi", "blue", False)
    # gameBoard.add_player("Jupiter", "red", True)

    if game_board.is_test:
        game_board.create_test_tiles()

    app = gui.App()
    app.connect(gui.QUIT, app.quit)

    cnt = gui.Container(align=-1, valign=-1)
    # cnt.add(player_dialog, 50, 0)
    app.init(cnt)

    clock = pygame.time.Clock()

    player_dialog = PlayerDialog(game_board)
    player_dialog.open()

    while player_dialog.is_open():
        player_dialog.interact(app)
        app.paint()
        pygame.display.flip()
        clock.tick(game_board.fps)
        game_board.total_frames += 1

    while True:
        game_board.draw()

        is_computer = game_board.player_deck.current_player.is_computer
        if (
            is_computer and
            game_board.player_deck.step is not None and
            game_board.status not in [
                GameBoardEnum.END, GameBoardEnum.NEW_GAME
            ]
        ):
            pygame.display.flip()

            game_board.pick_card()  # computer's turn
            game_board.draw(game_board.player_deck.step - 1)
            pygame.display.flip()
            if game_board.is_auto_focus:
                time.sleep(1)

        interaction(game_board, app, player_dialog)

        app.paint()
        pygame.display.flip()
        clock.tick(game_board.fps)
        game_board.total_frames += 1

        if game_board.status == GameBoardEnum.NEW_GAME:
            del game_board
            game_board = GameBoard(FPS, is_test=False)
            for player in org_player:
                game_board.add_player(*player[:3])
            # game_board.is_auto_focus = org_is_auto_focus

        if game_board.status == GameBoardEnum.END and AUTO_START:
            game_board.status = GameBoardEnum.NEW_GAME


if __name__ == '__main__':
    main()
