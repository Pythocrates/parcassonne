'''This module contains the PlayerDialog class.'''

from pgu.gui import (
    Button, Checkbox, Dialog, Form, Group, Input, Label, Select, Spacer, Table,
    CHANGE, CLICK, CLOSE, pguglobals
)
import pygame

from player_deck import PlayerDeck


class TableBuilder:
    def __init__(self, handler):
        self._handler = handler

    def _build_players_table(self, players):
        players_table = self._handler._players_table = Table()
        players_table.tr()
        players_table.td(Label("Name", align=1), align=-1)
        players_table.td(Spacer(width=8, height=8))
        players_table.td(Label("Color", align=1), align=-1)
        players_table.td(Spacer(width=8, height=8))
        players_table.td(Label("Computer", align=1), align=-1)

        for index, player in enumerate(players):
            self._handler._add_row(player, index)

    def _build_buttons_table(self, players):
        buttons_table = Table()
        buttons_table.tr()
        add_button = Button(
            "Add", align=0, disabled=len(players) >= PlayerDeck.max_players)
        self._handler._add_button = add_button
        add_button.connect(CLICK, self._handler.on_add_player)
        buttons_table.td(add_button, align=0)

        buttons_table.td(Spacer(width=10, height=8))

        reset_button = Button("Reset", align=0)
        reset_button.connect(CLICK, self._handler._on_reset)
        buttons_table.td(reset_button, align=0)

        buttons_table.td(Spacer(width=10, height=8))

        # The okay button CLICK event is connected to the Dailog's
        # send event method.  It will send a CHANGE event.
        start_button = Button(
            "Start", align=0, disabled=not self._handler._is_valid())
        start_button.connect(CLICK, self._handler.send, CHANGE)
        self._handler._start_button = start_button
        buttons_table.td(start_button, align=0)

        buttons_table.td(Spacer(width=10, height=8))

        close_button = Button("Quit", align=0)
        close_button.connect(CLICK, self._handler.send, CLOSE)
        buttons_table.td(close_button, align=0)

        return buttons_table

    def _build_options_table(self):
        options_table = Table()
        group = Group(name='Auto Focus', value=['enable'])
        group.connect(CHANGE, self._handler._on_checked, group.value)
        options_table.tr()
        options_table.td(Checkbox(group, 'enable'), align=-1)
        options_table.td(Label('Auto Focus', align=-1), align=-1)

        return options_table

    def _build_main_table(self, players):
        self._build_players_table(players)
        buttons_table = self._build_buttons_table(players)
        options_table = self._build_options_table()

        main_table = Table()
        main_table.tr()
        main_table.td(Spacer(width=8, height=8))

        main_table.tr()
        main_table.td(self._handler._players_table, colspan=1)

        main_table.tr()
        main_table.td(Spacer(width=8, height=8))

        main_table.tr()
        main_table.td(buttons_table, colspan=1)

        main_table.tr()
        main_table.td(options_table, colspan=-1)

        return main_table


class PlayerDialog(Dialog):
    def __init__(self, game_board, **params):
        self.game_board = game_board

        players = game_board.player_deck.players
        if not players:
            game_board.add_player('Player1', 'black', is_computer=False)

        # Once a form is created, all the widgets that are added with a name
        # are added to that form.
        self._form = Form()

        main_table = TableBuilder(handler=self)._build_main_table(players)
        super().__init__(title=Label("Configure Players"), main=main_table)

        self.connect(CHANGE, self._on_start)
        self.connect(CLOSE, self._on_cancel)

    def open(self, *args, **kwargs):
        super().open(*args, **kwargs)

        while self.is_open():
            for event in pygame.event.get():
                pguglobals.app.event(event)

            pguglobals.app.paint()
            pygame.display.flip()
            pygame.time.Clock().tick(self.game_board.fps)

    def _is_valid(self):
        """
        Is edited dialog content valid?
        Can it be started?
        """
        return self.game_board.player_deck.arePlayersValid()

    def _on_cancel(self):
        """
        Player configuration is cancelled, quit application
        """
        pygame.quit()

    def _on_start(self):
        """
        Pressed start in Playerdialog, start game after players are configured
        """
        # do not call on_cancel during close
        self.disconnect(CLOSE, self._on_cancel)
        self.close()  # close dialog, must be the last line

    def _on_checked(self, value):
        self.game_board.is_auto_focus = 'enable' in value

    def on_add_player(self):
        """
        Add a new player
        """
        players = self.game_board.player_deck.players
        index = len(players)
        if index == 2:
            self.game_board.add_player('Simon', 'black', is_computer=True)
        else:
            self.game_board.add_player(
                f'Player{index+1}', 'red', is_computer=True)
        player = players[-1]
        self._add_row(player, index)

        self._add_button.disabled = len(players) >= PlayerDeck.max_players
        self._start_button.disabled = not self._is_valid()

    def _on_name_changed(self, index, widget):
        player = self.game_board.player_deck.players[index]
        player.name = widget.value
        self._start_button.disabled = not self._is_valid()

    def _on_color_changed(self, index):
        player = self.game_board.player_deck.players[index]
        value = self._form[f'color{index}'].value
        player.set_color(value)
        self._start_button.disabled = not self._is_valid()

    def _on_player_type_changed(self, index):
        player = self.game_board.player_deck.players[index]
        player.is_computer = 1 in self._form[f'computer{index}'].value
        self._start_button.disabled = not self._is_valid()

    def _on_reset(self):
        """
        Delete all rows/players
        """
        self.game_board.player_deck.players = []
        self._players_table.clear()
        self.on_add_player()

    def _add_row(self, player, index):
        """
        Add a new row (player) to the table
        """
        players_table = self._players_table
        players_table.tr()

        input_widget = Input(name=f'name{index}', value=player.name, size=12)
        input_widget.connect(CHANGE, self._on_name_changed, index, input_widget)
        players_table.td(input_widget)

        players_table.td(Spacer(width=8, height=8))

        select_widget = Select(name=f'color{index}', value=player.color)
        select_widget.connect(CHANGE, self._on_color_changed, index)
        select_widget.add("Black", 'black')
        select_widget.add("Blue", 'blue')
        select_widget.add("Green", 'green')
        select_widget.add("Red", 'red')
        select_widget.add("Yellow", 'yellow')
        players_table.td(select_widget, colspan=1)

        players_table.td(Spacer(width=8, height=8))

        value = []
        if player.is_computer:
            value.append(1)
        group = Group(name=f'computer{index}', value=value)
        group.connect(CHANGE, self._on_player_type_changed, index)
        players_table.td(Checkbox(group, value=1))


