'''This module contains the PlayerDeck class.'''

import pygame

from enums import MiniPlayerStatusEnum, GameBoardEnum
from player import Player


class PlayerDeck(pygame.Rect):
    width, height = 6, 6
    border_topleft = 35, 31
    border_width, border_height = 150, 380
    max_players = 5
    playerSelectionColor = 212, 154, 67

    font_name_size = 15
    font_name_color = (255, 255, 255)
    # font_name_type = 'arial'
    font_score_size = 25
    font_score_color = (255, 255, 255)
    # font_score_type = 'monospace'

    # Simon added 20190126
    font_add_score_size = 25
    font_add_score_color = (255, 255, 255)
    # font_add_score_type = 'monospace'
    # End Simon 20190126

    def __init__(self, game_board):
        self.game_board = game_board

        self.players = []
        self.step = 0

    @property
    def name(self):
        return self.game_board.name

    @property
    def status(self):
        return self.game_board.status

    @property
    def total_frames(self):
        return self.game_board.total_frames

    # Add a new player to the game
    def add_player(self, name, color, is_computer):
        if len(self.players) < PlayerDeck.max_players:
            self.players.append(Player(self, name, color, is_computer))

    @property
    def current_player(self):
        return self.players[self.step % len(self.players)]

    # Is there at least one player?
    def hasPlayer(self):
        return len(self.players) > 0

    # Players are valid? All fields are filled and there is no color repetition
    def arePlayersValid(self):
        return (
            bool(players := self.players) and
            all(p.isValid() for p in players) and
            len(colors := [p.color for p in players]) == len(set(colors))
        )

    # Display all players
    def draw(self, step=None):
        if None is step:
            step = self.step

        screen = pygame.display.get_surface()

        for player in self.players:
            x, y = player.border_rect.topleft

            # figure
            if step % len(self.players) == self.players.index(player):
                screen.fill(PlayerDeck.playerSelectionColor, player.border_rect)
            screen.blit(player.image, (x, y))
            if player.is_computer:
                screen.blit(
                    Player.write(
                        'C',
                        PlayerDeck.font_name_size, PlayerDeck.font_name_color
                    ), (
                        x + player.image.get_rect().width // 3,
                        y + player.image.get_rect().height // 4
                    )
                )

            # name
            screen.blit(
                Player.write(
                    player.name,
                    PlayerDeck.font_name_size, PlayerDeck.font_name_color
                ), (
                    x + player.image.get_rect().width +
                    PlayerDeck.font_name_size // 4,
                    y
                )
            )

            # minifigures
            xx = x + player.image.get_rect().width
            yy = (
                y + player.image.get_rect().height -
                player.image_player_deck.get_rect().height - 1
            )

            for miniPlayer in player.getMiniPlayersByType(MiniPlayerStatusEnum.IN_BASE):
                screen.blit(miniPlayer.image_player_deck, (xx, yy))
                xx += player.image_player_deck.get_rect().width

            # score
            xx = x + player.image.get_rect().width + player.image_player_deck.get_rect().width * Player.max_mini_players
            yy = y + (player.image.get_rect().height - PlayerDeck.font_score_size) // 2
            screen.blit(Player.write('{:>3}'.format(player.score), PlayerDeck.font_score_size, PlayerDeck.font_score_color), (xx, yy))
            #func.text_to_screen(screen, '{:>3}'.format(player.score), xx, yy, PlayerDeck.font_score_size, PlayerDeck.font_score_color, PlayerDeck.font_score_type)

            # Simon added 20190126
            # add score
            xx = x + player.image.get_rect().width + player.image_player_deck.get_rect().width * 4
            yy = y + (player.image.get_rect().height - PlayerDeck.font_score_size) // 2 + PlayerDeck.font_add_score_size

            if self.status == GameBoardEnum.END:
                sc = player.endScore
            else:
                sc = player.addScore

            screen.blit(Player.write('+({:>3})'.format(sc), PlayerDeck.font_add_score_size, PlayerDeck.font_add_score_color), (xx, yy))
