'''This module contains the TileDeck class.'''

import gzip
import pickle
import sys

import pygame

import func
import load_img
from card_deck import CardDeck
from enums import (
    ButtonTypeEnum, CardStatusEnum, GameBoardEnum, TileType,
    MiniPlayerStatusEnum)
from card_tile_enum import CardTileEnum
from direction_enum import DirectionEnum
from tile import Tile
from button import Button
from card import Card
from player import Player
from coord import SubtileWorldCoord
from area import Area


class TileDeck(pygame.Rect):
    #width, height = 6, 6
    #border_topleft = 219, 31
    #border_width, border_height = 551, 538

    def __init__(self, game_board, offset, size, grid_size):
        super().__init__(offset, size)
        self.cols, self.rows = grid_size

        self.tile_width, self.tile_height = Card.width + 2, Card.height + 2

        self.game_board = game_board
        self.tiles = {}  # { (x,y) : tile }, where x,y is in tile position

        # center tile coordinate (in tile, not in pixel) of visible window
        self.center_x, self.center_y = 0, 0

        # containing tuples with ('+|-', xyts, miniplayer), where '+' is
        # insert, '-' is removement
        self.last_mini_players = []

        # { xyts : area, ...}, more info at function advancedSubtileSearch and
        # at area class
        self.area_by_player_subtile = {}

    @property
    def buttons(self):
        return self.game_board.buttons

    @property
    def grabbed_object(self):
        return self.game_board.grabbed_object

    @property
    def tile_deck(self):
        return self.game_board.tile_deck

    @property
    def total_frames(self):
        return self.game_board.total_frames

    def get_tile_by_status(self, status):
        """
        Returns the first tile with the given status of tile deck.
        """
        for tile in list(self.tiles.values()):
            if tile.status == status:
                return tile
        return None

    def add_tile(self, x, y, card, rotation, status, is_initial=False):
        """
        Create a new tile and add it to the tile deck.
        Parameters:
            x,y: left-top coordinate (in tile, not in pixel)
            card: to be inserted
            rotation: rotation of card
            status: initial status
            is_initial: this - normally first tile - invokes creation of areas
        Returns the created tile.
        """
        tile = Tile(self, x, y, self.tile_width, self.tile_height, status, card, rotation)

        self.tiles[(x, y)] = tile

        if is_initial:
            # to fill self.area_by_player_subtile at the start card
            for subtile_xy in tile.player_subtiles:
                xyts = SubtileWorldCoord((tile.get_xy(), subtile_xy))
                self.advancedSubtileSearch(xyts, isPseudo=False)

        return tile

    def get_deck_topleft(self):
        """
        Returns left top coordinate (in tile, not in pixel) of visible window
        of tile deck
        """
        x = self.center_x - self.cols // 2
        if self.cols % 2 == 0:
            x += 1
        y = self.center_y - self.rows // 2
        if self.rows % 2 == 0:
            y += 1
        return (x, y)

    def get_mouse_xy(self):
        """
        Returns left top coordinate (in tile, not in pixel) of tile pointed by
        mouse
        """
        mouse_pos = pygame.mouse.get_pos()
        x = (mouse_pos[0] - self.left) // self.tile_width
        y = (mouse_pos[1] - self.top) // self.tile_height
        deck_x, deck_y = self.get_deck_topleft()
        x += deck_x
        y += deck_y
        return (x, y)

    def get_tile_by_xy(self, xxx_todo_changeme):
        """
        Returns tile of tile deck with given coordinate (in tile, not in pixel)
        """
        (x, y) = xxx_todo_changeme
        return self.tiles.get((x, y))

    def getNumberOfSurroundedTiles(self, tile, withDiagonal=True):
        """
        Returns the number of the adjacent tiles of a tile.
        Checks the neighbours horizontally and vertically, but if withDiagonal
        is True, the diagonal directions are also checked.
        """
        surround = 0
        for position in Tile.get_surrounding_tile_positions(
            tile.get_xy(), withDiagonal
        ):
            adjacent_tile = self.get_tile_by_xy(position)
            if adjacent_tile is not None:  # there is an adjacent tile
                surround += 1
        return surround

    def isTileSurrounded(self, tile, withDiagonal=True):
        """
        Has tile all adjacent tiles?
        If withDiagonal is True, the diagonal directions are also checked.
        """
        return self.getNumberOfSurroundedTiles(tile) == (
            8 if withDiagonal else 4)

    def is_legal_new_tile(self, x, y):
        """
        Quick precheck, that a tile position is almost legal place for a new
        tile.
        """
        if len(self.tiles) == 0:  # tile deck is empty
            return True

        if (x, y) in self.tiles:  # position is already occupied by a tile
            return False

        is_legal_tile = False

        for position in Tile.get_surrounding_tile_positions((x, y)):
            tile = self.get_tile_by_xy(position)
            if tile is not None:  # there is an adjacent tile
                is_legal_tile = True
                break

        return is_legal_tile

    def is_border_legal_tile(self, almost_tile):
        """
        Prechecked (almost) tile is absolutely a legal place for a new tile?
        """
        if len(self.tiles) == 0:
            return True

        is_border_legal_tile = True

        for position in Tile.get_surrounding_tile_positions(
            almost_tile.get_xy()
        ):
            tile = self.get_tile_by_xy(position)
            if tile is None:
                continue
            if tile.yt - almost_tile.yt == -1:  # tile is north to almost_tile
                if not tile.get_subtile_south().simplified == almost_tile.get_subtile_north().simplified:
                    is_border_legal_tile = False
                    break
            if tile.xt - almost_tile.xt == 1:  # tile is east to almost_tile
                if not tile.get_subtile_west().simplified == almost_tile.get_subtile_east().simplified:
                    is_border_legal_tile = False
                    break
            elif tile.yt - almost_tile.yt == 1:  # tile is south to almost_tile
                if not tile.get_subtile_north().simplified == almost_tile.get_subtile_south().simplified:
                    is_border_legal_tile = False
                    break
            elif tile.xt - almost_tile.xt == -1:  # tile is west to almost_tile
                if not tile.get_subtile_east().simplified == almost_tile.get_subtile_west().simplified:
                    is_border_legal_tile = False
                    break

        return is_border_legal_tile

    def get_legal_almost_tiles(self, card, isOnlyFirst=False):
        """
        # Returns all almost_tiles that can be placed to the board,
        # rotated but "identical" tiles are omitted. For example
        # monastery is "identical" at every rotation, so no rotation
        # is used there.
        """
        legal_almost_tiles = {}  # { (x, y, rotation) : tile }

        for tile in self.tiles.values():
            for atile_x, atile_y in Tile.get_surrounding_tile_positions((tile.xt, tile.yt)):
                if not self.is_legal_new_tile(atile_x, atile_y):
                    continue
                for rotation in card.rotation_range:
                    if (atile_x, atile_y, rotation) not in legal_almost_tiles:
                        almost_tile = Tile(self, atile_x, atile_y, self.tile_width, self.tile_height, TileType.PSEUDO_TILE, card, rotation)
                        if self.is_border_legal_tile(almost_tile):
                            legal_almost_tiles[(atile_x, atile_y, rotation)] = almost_tile
                            if isOnlyFirst:
                                return legal_almost_tiles

        return legal_almost_tiles

    def fill_card_pair_cache(self):
        """
        Fills matching card pairs in all combinations.
        Returns a dictionary: { card_pair_id : { xys1 : xys2, ...}, ... }
        where card_pair_id is for example
        city_middle_border_w_crest-270-w-monastery-90.
        Using CARD_DECK_TEMPLATE there are 12816 pairs.
        This function is rather slow, it takes about 2 minutes on my test PC.
        """
        card_deck = self.game_board.card_deck
        card_pair_cache_dict = card_deck.card_pair_cache_dict
        uniqueCardDict = card_deck.uniqueCardDict
        filename = 'data/card_pairs.bin.pgz'

        if len(card_pair_cache_dict) > 0:
            return

        try:
            with gzip.GzipFile(filename, 'rb') as f:
                card_pair_cache_dict = pickle.load(f)
        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))

        if len(card_pair_cache_dict) == 0:

            card_deck.fill_card_pair_cache()
            card_pair_cache_dict = card_deck.card_pair_cache_dict

            for key in list(card_pair_cache_dict.keys()):
                self.tiles = {}

                keyList = key.split('-')
                card0_name = keyList[0]
                card0 = uniqueCardDict.get(card0_name)
                card0_rotation = int(keyList[1])
                direction = keyList[2]
                card1_name = keyList[3]
                card1 = uniqueCardDict.get(card1_name)
                card1_rotation = int(keyList[4])

                x, y = 0, 0
                tile0 = self.add_tile(
                    x, y, card0, card0_rotation, TileType.FIXED_TILE)

                if direction == DirectionEnum.NORTH.value:
                    y -= 1
                elif direction == DirectionEnum.EAST.value:
                    x += 1
                elif direction == DirectionEnum.SOUTH.value:
                    y += 1
                elif direction == DirectionEnum.WEST.value:
                    x -= 1

                tile1 = self.add_tile(
                    x, y, card1, card1_rotation, TileType.FIXED_TILE)
                xt, yt = tile0.get_xy()
                for subtile_xy, miniPlayer in list(tile0.player_subtiles.items()):
                    xyts = SubtileWorldCoord(((xt, yt), subtile_xy))
                    subtile = tile0.get_subtile(xyts.get_xys())
                    area = self.advancedSubtileSearch(xyts, isPseudo = True)
                    player_subtiles = area.player_subtiles
                    if len(player_subtiles) > 1:
                        xyts1, miniPlayer2 = list(player_subtiles.items())[0]
                        if xyts1 == xyts:
                            xyts1, miniPlayer2 = list(player_subtiles.items())[1]
                        card_pair_cache_dict.get(key)[xyts] = xyts1

            self.tiles = {}

            try:
                with gzip.GzipFile(filename, 'wb') as f:
                    pickle.dump(card_pair_cache_dict, f, 1)
            except IOError as e:
                print("I/O error({0}): {1}".format(e.errno, e.strerror))
            except:
                print("Unexpected error:", sys.exc_info()[0])
                raise

        self.game_board.card_deck.card_pair_cache_dict = card_pair_cache_dict

    def depositCard(self, card, targetTile=None, isPseudo=False):
        """
        Add a new tile to the tile_deck.
        A player places an card by mouse to the tile_deck,
        or a computer player already has a targetTile parameter.
        Computer may also deposit a pseudo card only for pre-calculation.
        """
        tile = None
        if card is None:
            raise ValueError

        if not isPseudo:
            self.deleteAllTileMarkedSubtiles()

        if targetTile == None:
            tile_x, tile_y = self.get_mouse_xy()
            if self.is_legal_new_tile(tile_x, tile_y):
                if len(list(self.tile_deck.tiles.values())) != 0:
                    tile = self.add_tile(tile_x, tile_y, card, 0, TileType.ALMOST_TILE)
                else:
                    tile = self.add_tile(tile_x, tile_y, card, 0, TileType.FIXED_TILE)
                card.status = CardStatusEnum.IN_TILE
                self.game_board.player_deck.current_player.append_last_tiles(tile)
        elif not isPseudo:
            tile = self.add_tile(targetTile.xt, targetTile.yt, targetTile.card, targetTile.rotation, TileType.FIXED_TILE, is_initial = True)# Simon modify is_initial = True at 20190206
            card.status = CardStatusEnum.IN_TILE
            self.game_board.player_deck.current_player.append_last_tiles(tile)
        elif isPseudo:
            tile = self.add_tile(targetTile.xt, targetTile.yt, targetTile.card, targetTile.rotation, TileType.PSEUDO_TILE)
            card.status = CardStatusEnum.IN_TILE

        return tile

    def removeTile(self, tile):
        """
        Removes ALMOST_TILE or PSEUDO_TILE tiles from tile_deck
        """
        if tile == None:
            raise ValueError
        if tile.status == TileType.ALMOST_TILE:
            del self.tiles[tile.get_xy()]
            self.game_board.player_deck.current_player.last_tiles.pop()
            self.game_board.status = GameBoardEnum.BACK_CARD
            self.game_board.restore_card() # todos
        elif tile.status == TileType.PSEUDO_TILE:
            del self.tiles[tile.get_xy()]

    def depositMiniPlayer(self, miniPlayer_xyts=None, isPseudo=False):
        """
        Add a miniPlayer to a subtile (pointed by the mouse) of the actual tile
        Check must be run that the subtile or the area is not already occupied.
        Returns true if miniPlayer is deposited, False if cancel, None if error and
        try again. If miniPlayer_xyts is not None, computer is on turn. Computer
        may move a isPseudo step for pre-calculation.
        """
        player = self.game_board.player_deck.current_player
        if miniPlayer_xyts == None:
            tile = player.last_tile
            # mouse position is inside tile
            mouse_x, mouse_y = pygame.mouse.get_pos()
            xs, ys = (mouse_x - tile.rect.x) // (self.tile_width // Card.subtile_width), (mouse_y - tile.rect.y) // (self.tile_height // Card.subtile_width)
            xs, ys = tile.simpleSubtileSearch((xs, ys))
        else:
            tile = self.get_tile_by_xy(miniPlayer_xyts.get_xyt())
            xs, ys = miniPlayer_xyts.get_xys()

        if not((xs, ys) in tile.player_subtiles and tile.player_subtiles.get((xs, ys)) == None):
            return False # already occupied area inside tile

        # additional verification
        xyts = SubtileWorldCoord((tile.get_xy(), (xs, ys)))
        area = self.advancedSubtileSearch(xyts, isPseudo, isMiniPlayerDeposited = True)
        if area.is_occupied():
            return None # already occupied tiles-area

        if miniPlayer_xyts == None:
            miniPlayer = self.grabbed_object
        else:
            miniPlayer = player.pickMiniPlayer()

        # deposit miniPlayer
        self.updateMiniPlayer(tile, xyts, miniPlayer)

        if isPseudo:
            self.last_mini_players.append(('-', xyts, miniPlayer))

        area.player_subtiles[xyts] = miniPlayer

        if not isPseudo:
            miniPlayer.status = MiniPlayerStatusEnum.IN_TILE

        self.analyseBoardByTile(tile, area, xyts, isPseudo)

        return True

    def restoreMiniPlayer(self, miniPlayer_xyts, miniPlayer=None, addScore=0, isPseudo=False, isEndGame = False):
        """
        Restore miniPlayer on miniPlayer_xyts provided that the miniPlayer exists on its tile.
        If miniPlayer is also set, the algorithm is quicker.
        """
        sourceTile = self.get_tile_by_xy(miniPlayer_xyts.get_xyt())
        if miniPlayer == None:
            for xys, miniPlayer in list(sourceTile.player_subtiles.items()):
                xyts = SubtileWorldCoord((sourceTile.get_xy(), xys))
                if miniPlayer != None and xyts == miniPlayer_xyts:
                    tile = self.get_tile_by_xy(xyts.get_xyt())

                    if isPseudo:
                        self.last_mini_players.append(('+', xyts, miniPlayer))
                    miniPlayer.player.add_score(addScore, isPseudo, isEndGame, miniPlayer)
                    # Simon modify 20190126
                    if not isEndGame:
                        self.updateMiniPlayer(tile, xyts, None) #restore minip
                        miniPlayer.status = MiniPlayerStatusEnum.IN_BASE
                    # End Simon 20190126
        else:
            tile, xyts = sourceTile, miniPlayer_xyts
            if tile.player_subtiles.get(xyts.get_xys()) == miniPlayer:

                if isPseudo:
                    self.last_mini_players.append(('+', xyts, miniPlayer))
                miniPlayer.player.add_score(addScore, isPseudo, isEndGame, miniPlayer)
                # Simon modify 20190126
                if not isEndGame:
                    self.updateMiniPlayer(tile, xyts, None) #restore minip
                    miniPlayer.status = MiniPlayerStatusEnum.IN_BASE
                # End Simon 20190126

    def restorePseudoMiniPlayers(self):
        """
        Computer player steps may insert/remove miniPlayers temporarily.
        These temporarily altered miniPlayes can be restored with this
        function. This function is counterpart of "restoreMiniPlayer".
        """
        while len(self.last_mini_players) > 0:
            operation, miniPlayer_xyts, miniPlayer = self.last_mini_players.pop()
            tile = self.get_tile_by_xy(miniPlayer_xyts.get_xyt())
            if operation == '+':
                # deposit miniPlayer
                self.updateMiniPlayer(tile, miniPlayer_xyts, miniPlayer)
                miniPlayer.status = MiniPlayerStatusEnum.IN_TILE
            elif operation == '-':
                # restore MiniPlayer
                self.updateMiniPlayer(tile, miniPlayer_xyts, None)
                miniPlayer.status = MiniPlayerStatusEnum.IN_BASE

    def checkMonasteries(self, tile, miniPlayer_xyts=None, isPseudo=False, isEndGame=False):
        """
        Check finished monasteries around a tile. Add scores to players.
        """
        xt, yt = tile.get_xy()
        if miniPlayer_xyts != None:
            subtile = tile.get_subtile(miniPlayer_xyts.get_xys())
            if subtile == CardTileEnum.MONASTERY_P:
                if not isEndGame:
                    if self.isTileSurrounded(tile):
                        self.restoreMiniPlayer(miniPlayer_xyts, miniPlayer=None, addScore=9, isPseudo=isPseudo)
                else:
                    surrounded = self.getNumberOfSurroundedTiles(tile)
                    if surrounded < 8:
                        self.restoreMiniPlayer(miniPlayer_xyts, miniPlayer=None, addScore=surrounded+1, isPseudo=isPseudo, isEndGame = isEndGame)
        if not isEndGame:
            # a deposited tile may create finished monasteries on its adjacent tiles
            for position in Tile.get_surrounding_tile_positions((xt, yt), withDiagonal=True):
                adjacentTile = self.get_tile_by_xy(position)
                if adjacentTile != None:
                    xys = adjacentTile.get_subtile_xy_by_subtile(CardTileEnum.MONASTERY_P)
                    if xys == None:
                        continue
                    if self.isTileSurrounded(adjacentTile):
                        xyts = SubtileWorldCoord((adjacentTile.get_xy(), xys))
                        self.restoreMiniPlayer(xyts, miniPlayer=None, addScore=9, isPseudo=isPseudo)

    def analyseBoardByTile(self, tile, miniPlayerArea=None, miniPlayer_xyts=None, isPseudo = False):
        """
        Analyse board after a tile has been deposited.
        It is also possible that a miniPlayer has been deposited too.
        Add scores to players.
        """
        xt, yt = tile.get_xy()
        if isPseudo:
            for player in self.game_board.player_deck.players:
                player.pseudoScore = player.score
        for subtile_xy, miniPlayer in list(tile.player_subtiles.items()):
            xyts = SubtileWorldCoord(((xt, yt), subtile_xy))
            subtile = tile.get_subtile(xyts.get_xys())
            if xyts == miniPlayer_xyts:
                area = miniPlayerArea
            else:
                area = self.advancedSubtileSearch(xyts, isPseudo)
            self.analyseBoardBySubtile(xyts, area, isPseudo)

        # check finished monasteries
        self.checkMonasteries(tile, miniPlayer_xyts, isPseudo)

        # last turn - extra calculation for scoring
        #if self.game_board.card_deck.isEmpty() and not isPseudo:
        #    self.analyseEndBoard(isPseudo)

    def analyseBoardBySubtile(self, xyts, area, isPseudo=False, isEndGame=False):
        """
        Analyse a player_subtile position of a deposited tile.
        If it is a part of a closed CITY or WAY area, remove miniPlayers on it,
        and add scores to players.
        """
        searchedTile = self.get_tile_by_xy(xyts.get_xyt())
        searchedSubtile = searchedTile.get_subtile(xyts.get_xys())

        if not isEndGame:
            if not area.is_closed():
                return
            if searchedSubtile not in (CardTileEnum.CITY_P, CardTileEnum.WAY_P):
                return
        else:
            if area.is_closed() and searchedSubtile in (CardTileEnum.CITY_P, CardTileEnum.WAY_P):
                return
            if searchedSubtile not in (CardTileEnum.CITY_P, CardTileEnum.WAY_P, CardTileEnum.FIELD_P):
                return
        if not area.is_occupied():
            return

        playerSumDict = {}  # { player1 : sum, player2 : sum, ...}
        miniPlayerCollect = {} # {player : miniPlayer}, Simon added 20190128
        crestSum = 0
        for xyts, miniPlayer in list(area.player_subtiles.items()):
            if searchedSubtile == CardTileEnum.CITY_P:
                tile = self.get_tile_by_xy(xyts.get_xyt())
                if tile.card.crest:
                    crestSum += 1
            if miniPlayer != None:
                player = miniPlayer.player
                playerSumDict[player] = playerSumDict.get(player, 0) + 1
                # Simon added 20190128
                miniPlayerCollect[player] = miniPlayer
                # End Simon 20190128

        if searchedSubtile == CardTileEnum.FIELD_P:
            cityAreas, coor_city_xyt = self.getClosedCityAreasSurroundedByFieldArea(area) # Simon modify 20190116
            #print 'found adjacent city areas', len(cityAreas)

        m = max(playerSumDict.values())
        winnerPlayers = [player for player in list(playerSumDict.keys()) if playerSumDict[player] == m]
        for player in winnerPlayers:
            if searchedSubtile == CardTileEnum.CITY_P:
                # Simon modify score to 1, 20190116
                if not isEndGame:
                    extraScore = len(area.player_subtiles)*2 + crestSum*2
                    player.add_score(extraScore, isPseudo)
                else:
                    extraScore = len(area.player_subtiles) + crestSum
                    #print('CITY_P', player.color, extraScore)#temp
                    player.add_score(extraScore, isPseudo, isEndGame, miniPlayerCollect[player])
            elif searchedSubtile == CardTileEnum.WAY_P:
                # Simon modify 20190130
                collect = set()
                for item in list(area.player_subtiles.items()):
                    if item:
                        it = item[0].get_xyt()
                        if it:
                            (x, y) = it
                            if type(x) == int and type(y) == int:
                                collect.add(it)

                extraScore = len(collect)
                # End Simon 20190130

                # if isEndGame: #temp
                #    print('WAY_P', player.color, extraScore)#temp
                # End Simon modify 20190116
                player.add_score(extraScore, isPseudo, isEndGame, miniPlayerCollect[player])
            elif searchedSubtile == CardTileEnum.FIELD_P:
                extraScore = len(cityAreas)*3
                #if isEndGame: #temp
                #    print('FIELD_P', player.color, extraScore)#temp
                #    print(coor_city_xyt)
                player.add_score(extraScore, isPseudo, isEndGame, miniPlayerCollect[player])

        # restore all miniPlayers
        if searchedSubtile in (CardTileEnum.CITY_P, CardTileEnum.WAY_P, CardTileEnum.FIELD_P):
            for xyts, miniPlayer in list(area.player_subtiles.items()):
                if miniPlayer != None:
                    self.restoreMiniPlayer(xyts, miniPlayer, addScore=0, isPseudo=isPseudo, isEndGame = isEndGame)

    def analyseEndBoard(self, isPseudo=False):
        """
        Analyse end board.
        Check occupied but unfinished cities, ways and monasteries.
        Check occupied fields and their adjacent closed but not occupied cities.
        Add scores to players.
        """
        statisticsBySubtile = {} # { subtile : count, ... }
        for area in set(self.area_by_player_subtile.values()):
            statisticsBySubtile[area.subtile] = statisticsBySubtile.get(area.subtile, 0) + 1
            if not area.is_closed() and area.subtile in (CardTileEnum.CITY, CardTileEnum.WAY) and area.is_occupied():
                # unfinished cities and ways
                xyts = list(area.player_subtiles.keys())[0]
                self.analyseBoardBySubtile(xyts, area, isPseudo, isEndGame = True)
            elif area.subtile == CardTileEnum.MONASTERY and area.is_occupied():
                # unfinished monasteries
                miniPlayer_xyts = list(area.player_subtiles.keys())[0]
                miniPlayerTile = self.get_tile_by_xy(miniPlayer_xyts.get_xyt())
                self.checkMonasteries(miniPlayerTile, miniPlayer_xyts, isPseudo, isEndGame = True)
            elif area.subtile == CardTileEnum.FIELD and area.is_occupied():
                # occupied fields and their adjacent closed but not occupied cities
                xyts = list(area.player_subtiles.keys())[0]
                self.analyseBoardBySubtile(xyts, area, isPseudo, isEndGame = True)
        #print 'statisticsBySubtile', statisticsBySubtile

    def advancedSubtileSearch(self, xyts, isPseudo=False, isMiniPlayerDeposited=False):
        """
        Search for subtiles inside more adjacent tiles where miniPlayer can be placed.
        Returns found subtile positions in a list, and an closed area flag:
        { "player_subtiles" : { xyts : miniPlayer | None, ...}, "other_subtiles" : [xyts, ...], 'missing_adjacent_tiles' : [xyt, ...], "isAreaClosed" : True | False, 'isAreaOccupied' : True | False}
        isAreaClosed := (len(missing_adjacent_tiles) == 0)
        At "way"-s, diagonal search is not allowed if there is an adjacent way-cross.
        Passing accross 2 diagonal ways is not allowed.
        """
        searched_xyts = xyts
        searchedTile = self.get_tile_by_xy(xyts.get_xyt())
        searchedSubtile = searchedTile.get_subtile(xyts.get_xys())
        searchedSubtileSimplified = searchedSubtile.simplified
        searchedSubtileExtended = searchedSubtile.extended

        area = Area(searchedSubtileSimplified)

        toSearch = set()
        toSearch.add(xyts)

        afterSearch = set()

        markedSubtiles = {} # { tile : { "isAreaClosed" : None, "subtiles" : [] } }

        while not len(toSearch) == 0:
            xyts = toSearch.pop()

            if xyts in afterSearch:
                continue

            afterSearch.add(xyts)

            tile = self.get_tile_by_xy(xyts.get_xyt())
            if tile == None:
                missing_adjacent_tiles = area.missing_adjacent_tiles
                if xyts.get_xyt() not in missing_adjacent_tiles:
                    missing_adjacent_tiles.append(xyts.get_xyt())
                continue

            if not isPseudo:
                if tile not in markedSubtiles:
                    markedSubtiles[tile] = { 'isAreaClosed' : None, 'subtiles' : [] }

            subtile = tile.get_subtile(xyts.get_xys())
            if subtile == searchedSubtileExtended:
                foundArea = self.area_by_player_subtile.get(xyts)
                if foundArea != None:
                    #print 'area', area
                    #print 'foundArea', foundArea
                    all_subtiles = foundArea.get_all_subtiles()
                    afterSearch |= set(all_subtiles)
                    if not isPseudo:
                        for found_xyts in all_subtiles:
                            found_tile = self.get_tile_by_xy(found_xyts.get_xyt())
                            if found_tile not in markedSubtiles:
                                markedSubtiles[found_tile] = { 'isAreaClosed' : None, 'subtiles' : [] }
                            if found_xyts.get_xys() not in markedSubtiles.get(found_tile).get('subtiles'):
                                markedSubtiles.get(found_tile).get('subtiles').append(found_xyts.get_xys())
                    # merge foundArea into area
                    #print('foundArea', foundArea)
                    area.add(foundArea, searched_xyts)
                    #print('mergedArea', area)
                    if not isPseudo:
                        for xyts in list(area.player_subtiles.keys()):
                            self.area_by_player_subtile[xyts] = area
                else:
                    miniPlayer = tile.player_subtiles.get(xyts.get_xys())
                    area.player_subtiles[xyts] = miniPlayer
                    if not isPseudo:
                        self.area_by_player_subtile[xyts] = area
                        if xyts.get_xys() not in markedSubtiles.get(tile).get('subtiles'):
                            markedSubtiles.get(tile).get('subtiles').append(xyts.get_xys())
            elif subtile == searchedSubtileSimplified:
                area.other_subtiles.append(xyts)
                if not isPseudo:
                    if xyts.get_xys() not in markedSubtiles.get(tile).get('subtiles'):
                        markedSubtiles.get(tile).get('subtiles').append(xyts.get_xys())
            else:
                continue

            for xd in [-1, 0, 1]:
                for yd in [-1, 0, 1]:
                    if abs(xd)+abs(yd) == 1: # north, east, south, west
                        isDirectionAllowed = True
                        if searchedSubtileSimplified == CardTileEnum.CITY and Card.is_border_coord(xyts.get_xys()):
                            if (xyts.get_xys()) in list(Card.border_direction_coords.values()):
                                # the only possibility to get out from the subtile cage
                                pass
                            else:
                                # cannot get out from the subtile cage
                                if Card.is_on_border_line_coord(xyts.get_xys(), DirectionEnum.NORTH) and yd < 0:
                                    isDirectionAllowed = False
                                elif Card.is_on_border_line_coord(xyts.get_xys(), DirectionEnum.EAST) and xd > 0:
                                    isDirectionAllowed = False
                                elif Card.is_on_border_line_coord(xyts.get_xys(), DirectionEnum.SOUTH) and yd > 0:
                                    isDirectionAllowed = False
                                elif Card.is_on_border_line_coord(xyts.get_xys(), DirectionEnum.WEST) and xd < 0:
                                    isDirectionAllowed = False
                        if isDirectionAllowed:
                            toSearch.add(xyts.get_add_xys((xd, yd)))
                    elif abs(xd)+abs(yd) == 2: # north-east, south-east, south-west, north-west
                        # Simon modify 20190121
                        #if searchedSubtileSimplified in (CardTileEnum.WAY, CardTileEnum.FIELD): # diagonal movement allowed only at these
                        if searchedSubtileSimplified in [CardTileEnum.WAY]: # diagonal movement allowed only at these
                        # End Simon 20190121
                            diagonal_tile1 = self.get_tile_by_xy(xyts.get_add_xys((xd, 0)).get_xyt())
                            if diagonal_tile1 != None:
                                try:
                                    diagonal_subtile1 = diagonal_tile1.get_subtile(xyts.get_add_xys((xd, 0)).get_xys()).simplified
                                except ValueError:
                                    diagonal_subtile1 = None
                            else:
                                diagonal_subtile1 = None
                            diagonal_tile2 = self.get_tile_by_xy(xyts.get_add_xys((0, yd)).get_xyt())
                            if diagonal_tile2 != None:
                                try:
                                    diagonal_subtile2 = diagonal_tile2.get_subtile(xyts.get_add_xys((0, yd)).get_xys()).simplified
                                except ValueError:
                                    diagonal_subtile2 = None
                            else:
                                diagonal_subtile2 = None
                            if searchedSubtileSimplified == CardTileEnum.WAY and (diagonal_subtile1 == CardTileEnum.WAY_CROSS or diagonal_subtile2 == CardTileEnum.WAY_CROSS):
                                # at "way"-s, diagonal search is not allowed if there is an adjacent way-cross
                                pass
                            elif searchedSubtileSimplified == CardTileEnum.FIELD and (diagonal_subtile1 in [CardTileEnum.WAY, CardTileEnum.WAY_CROSS] and diagonal_subtile2 in [CardTileEnum.WAY, CardTileEnum.WAY_CROSS]):
                                # field passing accross 2 diagonal ways is not allowed
                                pass
                            else:
                                toSearch.add(xyts.get_add_xys((xd, yd)))

        if not isPseudo:
            for xyts, miniPlayer in list(area.player_subtiles.items()):
                tile = self.get_tile_by_xy(xyts.get_xyt())
                isAreaClosed = area.is_closed()
                markedSubtiles.get(tile)['isAreaClosed'] = isAreaClosed
                isAreaOccupied = area.is_occupied()
                if (isAreaClosed and isAreaOccupied and searchedSubtileSimplified in (CardTileEnum.CITY, CardTileEnum.WAY, CardTileEnum.MONASTERY)) or isMiniPlayerDeposited:
                    if markedSubtiles.get(tile) not in tile.markedSubtiles:
                        tile.markedSubtiles.append(markedSubtiles.get(tile))

        return area

    def getComputerCalculatedStep(self, card):
        """
        Calculate computer step.
        Returns a tuple containing calculated tile and miniPlayer_xyts,
        where miniPlayer_xyts can be None.
        """
        legal_almost_tiles = self.tile_deck.get_legal_almost_tiles(card, isOnlyFirst = False)
        maxGain = None
        bestPseudoTile, best_xyts = None, None

        for legal_almost_tile in list(legal_almost_tiles.values()):
            # card is deposited as a tile with pseudoTile status
            pseudoTile = self.depositCard(card, legal_almost_tile, isPseudo = True)
            self.analyseBoardByTile(pseudoTile, miniPlayerArea = None, miniPlayer_xyts = None, isPseudo = True)
            # pseudo step
            gain, temp_xyts = self.analyseComputerCalculatedStep(pseudoTile, xyts = None) # Simon modify 20190117
            if maxGain == None or gain > maxGain:
                maxGain = gain
                bestPseudoTile = pseudoTile
                best_xyts = None
                #print "bestPseudoTile 1, best_xyts, maxGain", bestPseudoTile, best_xyts, maxGain
            self.restorePseudoMiniPlayers()
            player = self.game_board.player_deck.current_player
            pickedMiniPlayer = player.pickMiniPlayer()

            if pickedMiniPlayer != None:
                for xys, falseMiniPlayer in list(pseudoTile.player_subtiles.items()):
                    # falseMiniPlayer must be None, a new tile cannot have miniPlayer
                    xyts = SubtileWorldCoord((pseudoTile.get_xy(), xys))
                    #print(xyts)#temp 201l90113
                    #print 'new miniPlayer', pickedMiniPlayer, xys
                    isDeposited = self.depositMiniPlayer(xyts, isPseudo = True)

                    # Simon modify 20190117
                    #if not isDeposited:
                    #    continue
                    ## pseudo step
                    gain, select_xyts = self.analyseComputerCalculatedStep(pseudoTile, xyts, player, isDeposited)
                    # End Simon 20190117

                    #print(maxGain, gain)#temp 20190113
                    if maxGain == None or gain > maxGain:
                        maxGain = gain
                        bestPseudoTile = pseudoTile
                        # Simon modify 20190117
                        #best_xyts = xyts
                        best_xyts = select_xyts
                        # End Simon 20190117

                        #print(best_xyts)#temp
                        #print "bestPseudoTile 2, best_xyts, maxGain", bestPseudoTile, best_xyts, maxGain
                    self.restorePseudoMiniPlayers()
            self.removeTile(pseudoTile)

        #print "bestPseudoTile 3, best_xyts, maxGain", bestPseudoTile, best_xyts, maxGain
        #print(bestPseudoTile, best_xyts)
        return (bestPseudoTile, best_xyts)

    # Simon modify 20190114
    #def analyseComputerCalculatedStep(self, pseudoTile, xyts=None):
    # End Simon 20190114
    def analyseComputerCalculatedStep(self, pseudoTile, xyts=None, player=None, isDeposited=True):
        """
        Computer's turn: added pseudoTile and optionally added miniPlayer on xyts
        """
        #print 'analyseComputerCalculatedStep1', xyts
        actualPlayer = self.game_board.player_deck.current_player
        result = actualPlayer.pseudoScore - actualPlayer.score
        select_xyts = xyts

        city_related_tile = ['2_city_borders_on_edge', '2_city_borders', '3cross_w_city_border', 'city_border1', 'city_border2', 'city_border_w_curve1', 'city_border_w_curve2', 'city_border_w_way', 'city_edge', 'city_edge_w_crest_a_curve', 'city_edge_w_crest', 'city_edge_w_curve', 'city_main_w_crest', 'city_middle_border', 'city_middle_border_w_crest', 'city_middle_border_w_way', 'city_middle', 'city_middle_w_crest', 'city_w_crest_a_way']

        if xyts == None:
            #result = self.getNumberOfSurroundedTiles(pseudoTile, withDiagonal=False)*0.2
            result = 0
        elif isDeposited: #player should NOT be None
            # Simon added 20190114
            searchedTile = self.get_tile_by_xy(xyts.get_xyt())
            #print(searchedTile)
            searchedSubtile = searchedTile.get_subtile(xyts.get_xys())
            #print(searchedSubtile)
            miniPlayersInBase = player.getMiniPlayersByType(MiniPlayerStatusEnum.IN_BASE)

            area = self.advancedSubtileSearch(xyts, isPseudo=True)
            wp = self.whoOccupiedCity(area)

            #if searchedSubtile == CardTileEnum.WAY_P: # temp for test only
            #    print(player.color) # temp
            #    for xyts, miniPlayer in list(area.player_subtiles.items()):# temp
            #        print(xyts.get_xyt())
            #        print(miniPlayer)

            if len(miniPlayersInBase) > 1 or self.game_board.player_deck.step > 60:
                if wp != None:
                    pcs = [pc.color for pc in wp]
                else:
                    pcs = None

                if pcs != None and player.color in pcs:
                    related_score = 2.3
                elif None == pcs:
                    related_score = 0
                elif searchedTile.card.name in city_related_tile:
                    related_score = -2
                else:
                    related_score = 0

                if searchedSubtile == CardTileEnum.CITY_P:
                    if searchedTile.card.name in ['2_city_borders_on_edge', '2_city_borders']:
                        result += related_score + 0.5
                    else:
                        result += 2
                else:
                    if searchedSubtile == CardTileEnum.FIELD_P:
                        cityAreas, coor_city_xyt = self.getClosedCityAreasSurroundedByFieldArea(area)
                        s = len(cityAreas) * 1.5
                        if 0 == s:
                            select_xyts = None
                        return result + s + related_score, select_xyts
                    elif searchedSubtile == CardTileEnum.WAY_P:
                        return (result + len(set(area.player_subtiles))* 0.5) +  related_score, select_xyts
                    elif searchedSubtile == CardTileEnum.MONASTERY_P:
                            s = self.getNumberOfSurroundedTiles(searchedTile) + 1
                            return result+s, select_xyts
                    else:
                        result += related_score
                        select_xyts = None
                        return result, select_xyts
            else:
                if wp != None:
                    pcs = [pc.color for pc in wp]
                else:
                    pcs = None

                if pcs != None and player.color in pcs:
                    related_score = 2
                    select_xyts = None
                elif None == pcs:
                    related_score = 0
                elif searchedTile.card.name in city_related_tile:
                    related_score = -2
                    select_xyts = None
                else:
                    related_score = 0

                if searchedSubtile == CardTileEnum.CITY_P:
                    result = result + 0.5 + related_score
                    select_xyts = xyts
                elif searchedSubtile == CardTileEnum.MONASTERY_P:
                    s = self.getNumberOfSurroundedTiles(searchedTile) + 1
                    result += s
                    select_xyts = xyts
                elif searchedSubtile == CardTileEnum.FIELD_P:
                    cityAreas, coor_city_xyt = self.getClosedCityAreasSurroundedByFieldArea(area)
                    s = len(cityAreas) * 1.5
                    select_xyts = xyts
                    if s < 2:
                        select_xyts = None
                    result = result + s + related_score
                else:
                    result += related_score

        else: # False == isDeposited
            select_xyts = None

            searchedTile = self.get_tile_by_xy(xyts.get_xyt())

            result = actualPlayer.pseudoScore - actualPlayer.score

            if searchedTile.card.name in city_related_tile:
                #temp
                #xyt = xyts.get_xyt()
                #print(xyt)
                #end temp
                area = self.advancedSubtileSearch(xyts, isPseudo=True)
                wp = self.whoOccupiedCity(area)
                if wp != None:
                    pcs = [pc.color for pc in wp]
                else:
                    pcs = None

                if pcs != None and player.color in pcs:
                    result += 2.3
                elif result < 1 and pcs != None: # different color
                    result -= 2

        #print 'analyseComputerCalculatedStep2 result', result
        return result, select_xyts
        # End Simon 20190114

        # Simon remark 20190117
        #h = 0
        #for player in self.game_board.playerDeck.players:
        #    if player == actualPlayer:
        #        h += player.pseudoScore - player.score
        #    else:
        #        h -= player.pseudoScore - player.score
        #
        ##print 'score', player.score, 'pseudoScore', player.pseudoScore
        #return h
        # End Simon 20190117

    def whoOccupiedCity(self, area):
        """
        Simon added, only for who Occupied CITY_P
        """
        if len(area.player_subtiles) == 0:
            return None

        playerSumDict = {}

        for xyts, miniPlayer in list(area.player_subtiles.items()):
            searchedTile = self.get_tile_by_xy(xyts.get_xyt())
            if None == searchedTile:
                return None
            searchedSubtile = searchedTile.get_subtile(xyts.get_xys())
            if searchedSubtile == CardTileEnum.CITY_P and miniPlayer != None:
                player = miniPlayer.player
                playerSumDict[player] = playerSumDict.get(player, 0) + 1
                #return miniPlayer.player.color

        if len(playerSumDict) > 0:
            m = max(playerSumDict.values())
            winnerPlayers = [player for player in list(playerSumDict.keys()) if playerSumDict[player] == m]
            return winnerPlayers
        else:
            return None

    def getClosedCityAreasSurroundedByFieldArea(self, fieldArea):
        """
        Returns all city areas touched by the given field area
        """
        city_areas = []
        coor_city_xyt = [] #Simon added for bug trace 20190116
        for xyts, miniPlayer in list(fieldArea.player_subtiles.items()):
            tile = self.get_tile_by_xy(xyts.get_xyt())
            if tile: # Simon added 20190206
                foundSubtileCities = tile.simpleSubtileSearch(xyts.get_xys(), isFieldToCity = True)
                for foundSubtile in foundSubtileCities:
                    city_xyts = SubtileWorldCoord((xyts.get_xyt(), foundSubtile))
                    cityArea = self.area_by_player_subtile.get(city_xyts)
                    if cityArea != None and cityArea.is_closed():
                        if cityArea not in city_areas:
                            #print 'cityArea', cityArea
                            city_areas.append(cityArea)
                            coor_city_xyt.append(xyts.get_xyt()) # Simon added 20190116
        return city_areas, coor_city_xyt # Simon modify 20190116

    def button_rotate_left(self):
        """
        On an "almost_card" tile rotate the tile.
        """
        tile = self.get_tile_by_status(TileType.ALMOST_TILE)
        if tile is not None:
            tile.rotation += 90
            tile.rotation %= 360
            tile.update_subtiles()

    def button_rotate_right(self):
        """
        On an "almost_card" tile rotate the tile.
        """
        tile = self.get_tile_by_status(TileType.ALMOST_TILE)
        if tile is not None:
            tile.rotation -= 90
            tile.rotation %= 360
            tile.update_subtiles()

    def button_ok(self):
        """
        On an "almost_card" tile OK was clicked. It can become into a deposited
        real card tile.
        """
        tile = self.get_tile_by_status(TileType.ALMOST_TILE)
        if tile is not None:
            if self.is_border_legal_tile(tile):
                tile.status = TileType.FIXED_TILE
                tile.card.status = CardStatusEnum.IN_TILE
                # Simon added at 20190206
                self.depositCard(self.grabbed_object, tile)
                # End Simon 20190206
                self.game_board.status = GameBoardEnum.CARD_IN_TILE
                self.game_board.grabMiniPlayer()

    def button_cancel(self):
        """
        On an "almost_tile" Cancel was clicked. It must be deleted.
        """
        tile = self.get_tile_by_status(TileType.ALMOST_TILE)
        if tile is not None:
            self.removeTile(tile)

    def deleteButtons(self):
        """
        Delete buttons around tile deposition
        """
        for name in ('rotate_left', 'rotate_right', 'ok', 'cancel'):
            if name in self.buttons:
                del self.buttons[name]

    def scroll_north(self):
        """
        Scroll visible window of tile deck into north
        """
        self.center_y -= 1

    def scroll_east(self):
        """
        Scroll visible window of tile deck into east
        """
        self.center_x += 1

    def scroll_south(self):
        """
        Scroll visible window of tile deck into south
        """
        self.center_y += 1

    def scroll_west(self):
        """
        Scroll visible window of tile deck into west
        """
        self.center_x -= 1

    def scroll_player(self, player):
        """
        Scroll visible window of tile deck into the given player
        """
        last_tile = player.last_tile
        if last_tile is not None:
            self.center_x, self.center_y = last_tile.xt, last_tile.yt

    def deleteAllTileMarkedSubtiles(self):
        """
        Erase all markedSubtiles of all tiles on tile deck
        """
        for tile in list(self.tiles.values()):
            tile.markedSubtiles = []

    def updateMiniPlayer(self, tile, miniPlayer_xyts, mini_player):
        """
        Deposit or remove mini_player on a tile
        """
        tile.player_subtiles[miniPlayer_xyts.get_xys()] = mini_player
        area = self.area_by_player_subtile.get(miniPlayer_xyts)
        if area is not None:
            area.player_subtiles[miniPlayer_xyts] = mini_player

    def draw(self):
        """
        Display tile deck
        """
        screen = pygame.display.get_surface()
        screen.fill( (0,0,0), self)
        self.deleteButtons()

        deck_x, deck_y = self.get_deck_topleft()

        # draw blank cards everywhere inside visible window of tile deck
        for relative_x in range(0, self.cols):
            for relative_y in range(0, self.rows):
                x = self.left + relative_x * self.tile_width
                y = self.top + relative_y * self.tile_height
                screen.blit(CardDeck.cardBack.image, (x, y))

        # draw existing tiles of visible window of tile deck
        for tile in list(self.tiles.values()):
            deck_x, deck_y = self.get_deck_topleft()
            relative_x = tile.xt - deck_x
            relative_y = tile.yt - deck_y

            if (relative_x >= 0 and relative_x < self.cols and relative_y >= 0 and relative_y < self.rows):
                x = self.left + relative_x * self.tile_width
                y = self.top + relative_y * self.tile_height
            else:
                continue

            ##tileImg = pygame.image.load(CardDeck.cardFilePath + tile.card.file_name)
            #tileImg = tile.card.image
            ##tileImg = pygame.transform.rotate(tileImg, tile.rotation) # CCW
            tileImg = load_img.get_tile_image(tile.card.name, tile.rotation)
            screen.blit(tileImg, (x, y))

            # temporarily draw coordinates on tile
            #if True: # Simon change to False 20190129
            if False:
                func.text_to_screen(screen, "{0}".format(tile.get_xy()), x, y, 12, (0,0,0))

            # temporarily draw grids on tile
            if False:
                #func.text_to_screen(screen, tile.card.name, x, y, 8, (0,0,0))
                for n in range(1, Card.subtile_width):
                    pygame.draw.line(screen, (0,0,0), (x+(self.tile_width//Card.subtile_width)*n,y), (x+(self.tile_width//Card.subtile_width)*n,y+self.tile_height-1))
                for n in range(1, Card.subtile_height):
                    pygame.draw.line(screen, (0,0,0), (x,y+(self.tile_height//Card.subtile_height)*n), (x+self.tile_width-1,y+(self.tile_height//Card.subtile_height)*n))
                for xm in range(0, 5):
                    for ym in range(0, 5):
                        func.text_to_screen(screen, tile.card.get_subtile((xm,ym), tile.rotation).value, x+(self.tile_width//5)*xm+2, y+(self.tile_height//5)*ym+2, 16, (0,0,0))

            # draw border of tile with actual player's color
            for player in self.game_board.player_deck.players:
                if player.last_tile == tile:
                    border_width = 2
                    rect = ((x,y), (self.tile_width-border_width, self.tile_width-border_width))
                    tile.rect.topleft = (x,y)
                    pygame.draw.rect(screen, Player.color_rgb.get(player.color), rect, border_width)
                    break

            if tile.status == TileType.ALMOST_TILE:

                is_border_legal_tile = self.is_border_legal_tile(tile)

                #img_rotate_left = pygame.image.load(self.imgFilePath + 'rotate_left.png')
                img_rotate_left = load_img.str_to_img('i_rotate_left')
                img_rotate_left_rect = img_rotate_left.get_rect()
                img_rotate_left_rect.topleft = (x, y)
                button = Button('rotate_left', ButtonTypeEnum.ICON, img_rotate_left, img_rotate_left_rect.topleft, self.button_rotate_left)
                self.buttons['rotate_left'] = button

                #img_rotate_right = pygame.image.load(self.imgFilePath + 'rotate_right.png')
                img_rotate_right = load_img.str_to_img('i_rotate_right')
                img_rotate_right_rect = img_rotate_right.get_rect()
                img_rotate_right_rect.topleft = (x + tileImg.get_rect().width - img_rotate_right_rect.width, y)
                button = Button('rotate_right', ButtonTypeEnum.ICON, img_rotate_right, img_rotate_right_rect.topleft, self.button_rotate_right)
                self.buttons['rotate_right'] = button

                if 'ok' in self.buttons:
                    del self.buttons['ok']
                if is_border_legal_tile:
                    #img_ok = pygame.image.load(self.imgFilePath + 'check.png')
                    img_ok = load_img.str_to_img('i_check')
                    img_ok_rect = img_ok.get_rect()
                    img_ok_rect.topleft = (x, y + tileImg.get_rect().height - img_ok_rect.height)
                    button = Button('ok', ButtonTypeEnum.ICON, img_ok, img_ok_rect.topleft, self.button_ok)
                    self.buttons['ok'] = button

                #img_cancel = pygame.image.load(self.imgFilePath + 'cancel.png')
                img_cancel = load_img.str_to_img('i_cancel')
                img_cancel_rect = img_cancel.get_rect()
                img_cancel_rect.topleft = (x + tileImg.get_rect().width - img_cancel_rect.width, y + tileImg.get_rect().height - img_cancel_rect.height)
                button = Button('cancel', ButtonTypeEnum.ICON, img_cancel, img_cancel_rect.topleft, self.button_cancel)
                self.buttons['cancel'] = button

                if not is_border_legal_tile:
                    rect = pygame.Rect((0,0), (Card.width, Card.height))
                    rect.topleft = (x, y)
                    screen.fill((255,128,128,128), rect.clip(screen.get_rect()), pygame.BLEND_RGBA_MULT )

            elif tile.status == TileType.FIXED_TILE:
                for markedSubtiles in tile.markedSubtiles:
                    isAreaClosed = markedSubtiles.get('isAreaClosed')
                    for subtile_xy in markedSubtiles.get('subtiles', []):
                        subtile_x, subtile_y = tile.get_subtile_topleft_xy(subtile_xy)
                        subtile_x += x
                        subtile_y += y

                        rect = pygame.Rect((0,0), (Card.width // Card.subtile_width, Card.height // Card.subtile_height))
                        rect.topleft = (subtile_x, subtile_y)
                        if isAreaClosed:
                            color = (192,192,255,128)
                        else:
                            color = (255,192,192,128)
                        screen.fill( color, rect.clip(screen.get_rect()), pygame.BLEND_RGBA_MULT )

                for subtile_xy, miniPlayer in list(tile.player_subtiles.items()):
                    if miniPlayer != None:
                        subtile_x, subtile_y = tile.get_subtile_topleft_xy(subtile_xy)
                        subtile_x += x
                        subtile_y += y
                        screen.blit(miniPlayer.image_subtile, (subtile_x, subtile_y))

                        # Simon added 20190128
                        if miniPlayer.eScore > 0: # It will show at end game
                            score_height = 9
                            font_size = 10
                            font_color = (0, 0, 0) #black
                            font_type = 'arial'

                            pygame.draw.rect(screen, (255, 255, 255), (subtile_x, subtile_y, miniPlayer.image_subtile.get_width(), score_height))

                            screen.blit(Player.write('{:>3}'.format(miniPlayer.eScore), font_size, font_color), (subtile_x + miniPlayer.image_subtile.get_width()//2 - 5, subtile_y - 1))
                        # End Simon 20190128
