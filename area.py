'''This module contains the Area class.'''


class Area:
    def __init__(
            self, subtile=None, player_subtiles=None, other_subtiles=None,
            missing_adjacent_tiles=None
    ):
        self.subtile = subtile
        self.player_subtiles = player_subtiles or dict()
        self.other_subtiles = other_subtiles or list()
        self.missing_adjacent_tiles = missing_adjacent_tiles or list()

    def __str__(self):
        return (
            '{0}: subtile = {1}, player_subtiles = {2}, other_subtiles = {3}, '
            'missing_adjacent_tiles = {4}, isAreaClosed = {5}, isAreaOccupied '
            '= {6}'.format(
                self.__class__.__name__, self.subtile,
                self.player_subtiles_to_str(self.player_subtiles),
                self.other_subtiles, self.missing_adjacent_tiles,
                self.is_closed(), self.is_occupied()
            )
        )

    @staticmethod
    def player_subtiles_to_str(player_subtiles):
        items = ', '.join(
            f'{xyts}: {miniPlayer}'
            for xyts, miniPlayer in player_subtiles.items()
        )
        return f'{{{items}}}'

    def get_all_subtiles(self):
        """
        Returns all subtiles belongs to the area.
        """
        return list(self.player_subtiles.keys()) + self.other_subtiles

    def is_closed(self):
        """
        Is area closed? It means that no more card can be placed into the area.
        """
        return not self.missing_adjacent_tiles

    def is_occupied(self):
        """
        Is there at least one miniPlayer in the area?
        """
        return any(self.player_subtiles.values())

    def add(self, other, searched_xyts=None):
        """
        Merge addedArea into the area.
        """
        self.player_subtiles = {
            **self.player_subtiles, **other.player_subtiles}

        self.other_subtiles = list(
            set(self.other_subtiles + other.other_subtiles))
        self.missing_adjacent_tiles = list(
            set(self.missing_adjacent_tiles + other.missing_adjacent_tiles)
        )
        if (
                searched_xyts
                and searched_xyts.get_xyt() in self.missing_adjacent_tiles
        ):
            self.missing_adjacent_tiles.remove(searched_xyts.get_xyt())
