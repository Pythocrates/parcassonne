'''This module contais the enums.'''

from enum import auto, Enum


class ButtonTypeEnum(Enum):
    ICON = auto()


class CardStatusEnum(Enum):
    IN_DECK = auto()
    GRABBED = auto()
    BACK_DECK = auto()
    IN_TILE = auto()
    TRASH = auto()  # illegal card, it cannot be deposited to the board


class PlayerStatusEnum(Enum):
    IN_BASE = auto()


class MiniPlayerStatusEnum(Enum):
    IN_BASE = auto()
    GRABBED = auto()
    BACK_BASE = auto()
    ALMOST_SET = auto()
    IN_TILE = auto()


class GameBoardEnum(Enum):
    DECK = auto()
    CARD_GRABBED = auto()
    BACK_CARD = auto()
    CARD_ALMOST_TILE = auto()
    CARD_IN_TILE = auto()
    FIGURE_GRABBED = auto()
    END = auto()
    NEW_GAME = auto()


class TileType(Enum):
    # human player deposited card, but it still can be rotated and must be
    # confirmed
    ALMOST_TILE = auto()
    # computer deposited card for calculation, it is always restored
    PSEUDO_TILE = auto()
    FIXED_TILE = auto()
