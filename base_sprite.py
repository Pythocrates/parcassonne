'''This module contains the BaseSprite class.'''

import pygame


class BaseSprite(pygame.sprite.Sprite):

    groups = pygame.sprite.Group()

    def __init__(self, x, y, image, add_groups):
        super().__init__()

        if add_groups:
            self.groups.add(self)

        self.image = image

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def destroy(self, class_name):
        class_name.groups.remove(self)
        self.groups.remove(self)
        self.kill()
        del self

    def add_sprite(self):
        if not self.groups.has(self):
            self.groups.add(self)

    def remove_sprite(self):
        if self.groups.has(self):
            self.groups.remove(self)
