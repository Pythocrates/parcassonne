Carcassonne is classic board game.
Parcassonne is Carcassonne clone written in python 3.
This app(parcassonne) base on pycassonne 0.9.1.
Parcassonne try to enhance AI, but it still very weak.
So I suggest to 1 human vs 4 computer players.

Pycassonne URL below:
https://sourceforge.net/projects/pycassonne/

Environment:
Python 3.7 + Pygame 1.9.4 + PGU

Chinese Readme below:

遊戲名稱：帕卡頌 (Parcassonne)

源起：
帕卡頌是知名桌遊卡卡頌的PC單機複製版，
遊戲規則與卡卡頌的規則相同，
帕卡頌是從pycassonne 0.9.1改過來的。
目前AI仍然很弱，所以個人是建議：
一個人類玩家，對決4個電腦玩家。

遊戲規則：
可以自行Google 卡卡頌，
或者參考以下網址：
http://crazycat1130.pixnet.net/blog/post/1345560-【桌上遊戲】卡卡頌（carcassonne）

帕卡頌操作方法：
一開始設定玩家，是幾個人？幾個電腦？
注意，所有玩家都需要不同顏色，才能開始。
另外預設是勾選Auto Focus, 不勾選可關閉。
開始後，玩家點選左下角，板塊堆可以拿取地圖板塊，
放置好地圖板塊後，玩家需要點選小人(米寶)的位子，
小人的位子，只能在玩家剛剛放入的板塊，
如果點其它位子，代表不放小人。
如果小人的位子不合法，地圖上會顯示紅色區域，
代表衝到的區域。
如果小人的位子合法，就會放入小人。

另外按"上下左右"方向鍵，可以移動地圖視野。
用滑鼠點選玩家名字旁的大米寶圖案，
可以Focus到該顏色玩家最後放下的地圖板塊。
其餘玩法，跟卡卡頌一樣。

已知問題：

1. 如果在板塊上的道路有彎曲，在小人(米寶)放置時，
點擊彎曲處是不被當作是道路，而被當作是草原的。

2. 地圖板塊3cross_w_city_border, 在小人(米寶)放置到城堡(城市)時，
需要儘量點擊城堡內部一點，點擊城堡外圍，
因為要修正算分問題，會被視為點擊草原。

3. 玩太久，會出現Out of memory問題，從v0.9.7開始，嘗試重複使用圖檔，
已獲得一些改善。