'''This module contains the DirectionEnum class.'''

from enum import Enum


class DirectionEnum(Enum):
    NORTH = 'n'
    EAST = 'e'
    SOUTH = 's'
    WEST = 'w'
    NORTHEAST = 'ne'
    SOUTHEAST = 'se'
    SOUTHWEST = 'sw'
    NORTHWEST = 'nw'

    @property
    def opposite(self):
        """
        Get opposite direction
        """
        return {
            DirectionEnum.NORTH: DirectionEnum.SOUTH,
            DirectionEnum.EAST: DirectionEnum.WEST,
            DirectionEnum.SOUTH: DirectionEnum.NORTH,
            DirectionEnum.WEST: DirectionEnum.EAST,
            DirectionEnum.NORTHEAST: DirectionEnum.SOUTHWEST,
            DirectionEnum.SOUTHEAST: DirectionEnum.NORTHWEST,
            DirectionEnum.SOUTHWEST: DirectionEnum.NORTHEAST,
            DirectionEnum.NORTHWEST: DirectionEnum.SOUTHEAST
        }[self]
