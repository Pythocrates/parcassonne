'''This module contains the MiniPlayer class.'''

import pygame

from enums import MiniPlayerStatusEnum
from base_sprite import BaseSprite


class MiniPlayer(BaseSprite):
    def __init__(self, player):
        self.player = player
        self.eScore = 0  # Simon added 20190128
        super().__init__(x=0, y=0, image=self.image_subtile, add_groups=False)
        self.status = MiniPlayerStatusEnum.IN_BASE

    def __del__(self):
        del self.player
        del self.eScore
        del self

    def __str__(self):
        return '{0}: status = {1}'.format(self.__class__.__name__, self.status)

    @property
    def image_player_deck(self):
        return self.player.image_player_deck

    @property
    def image_subtile(self):
        return self.player.image_subtile

    @property
    def total_frames(self):
        return self.player.total_frames

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if status == MiniPlayerStatusEnum.IN_BASE:
            self.remove_sprite()
        elif status == MiniPlayerStatusEnum.GRABBED:
            self.image = self.image_subtile
            self.add_sprite()
        elif status == MiniPlayerStatusEnum.BACK_BASE:
            pass
        elif status == MiniPlayerStatusEnum.ALMOST_SET:
            self.remove_sprite()
        elif status == MiniPlayerStatusEnum.IN_TILE:
            self.remove_sprite()
        else:
            raise ValueError

        self._status = status
        self.__status_frames__ = self.total_frames

    def update(self, seconds):
        # no need for seconds but the other sprites need it
        if self.status == MiniPlayerStatusEnum.GRABBED:
            self.rect.center = pygame.mouse.get_pos()
