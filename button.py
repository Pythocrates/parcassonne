'''This module contains the Button class.'''

import pygame


class Button(pygame.Rect):
    def __init__(self, name, type, img, topleft, action):
        rect = img.get_rect()
        rect.topleft = topleft

        super().__init__(rect)

        self.name = name
        self.img = img
        self.type = type
        self.action = action

    def __str__(self):
        return '{0}: name = {1}, type = {2}, img = {3}, rect = {4}'.format(
            self.__class__.__name__, self.name, self.type, self.img, self.rect)

    def draw(self):
        """
        Display button
        """
        screen = pygame.display.get_surface()
        screen.blit(self.img, self.topleft)
