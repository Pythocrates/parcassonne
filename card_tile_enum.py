'''This module contains the CardTileEnum class.'''

import unittest
from enum import Enum


class CardTileEnum(Enum):
    CITY = 'C'
    FIELD = 'F'
    WAY = 'W'
    MONASTERY = 'M'
    CITY_P = 'c'
    FIELD_P = 'f'
    WAY_P = 'w'
    MONASTERY_P = 'm'
    WAY_CROSS = 'X'

    @property
    def simplified(self):
        '''Returns simplified CardTileEnum, remove postfix.'''
        if self.name.endswith(self.EXTENDED_FLAG):
            result = type(self)[self.name[:-len(self.EXTENDED_FLAG)]]
        else:
            result = self

        return result

    @property
    def extended(self):
        '''Returns extended CardTileEnum, add postfix.'''
        if (
                self.name.endswith(self.EXTENDED_FLAG) or
                self is type(self).WAY_CROSS
        ):
            result = self
        else:
            result = type(self)[self.name + self.EXTENDED_FLAG]

        return result

    @property
    def is_passable(self):
        '''Minifigure can be placed to this subtile type?'''
        return self is not CardTileEnum.WAY_CROSS


CardTileEnum.EXTENDED_FLAG = '_P'


class IsCardTileEnumTests(unittest.TestCase):
    """
    Unit test cases
    """
    def test1(self):
        card_tile_enum = CardTileEnum.CITY_P
        card_tile_enum = card_tile_enum.simplified
        self.assertEqual(card_tile_enum, CardTileEnum.CITY)

    def test2(self):
        card_tile_enum = CardTileEnum.CITY
        card_tile_enum = card_tile_enum.extended
        self.assertEqual(card_tile_enum, CardTileEnum.CITY_P)

    def test3(self):
        card_tile_enum = CardTileEnum.WAY_CROSS
        card_tile_enum = card_tile_enum.simplified
        self.assertEqual(card_tile_enum, CardTileEnum.WAY_CROSS)

    def test4(self):
        card_tile_enum = CardTileEnum.WAY_CROSS
        card_tile_enum = card_tile_enum.extended
        self.assertEqual(card_tile_enum, CardTileEnum.WAY_CROSS)

    def test5(self):
        card_tile_enum = CardTileEnum.WAY_CROSS
        passable = card_tile_enum.is_passable
        self.assertFalse(passable)

    def test6(self):
        card_tile_enum = CardTileEnum.WAY
        passable = card_tile_enum.is_passable
        self.assertTrue(passable)

    def test7(self):
        card_tile_enum = CardTileEnum.WAY_CROSS
        char = card_tile_enum.value
        self.assertEqual(char, "X")
