'''This module contains the interaction functionality.'''

import sys

import pygame

from game_board import GameBoardEnum
from card_deck import CardDeck


def interaction(game_board, app, player_dialog):
    # screen = pygame.display.get_surface()

    for event in pygame.event.get():

        mouse_pos = pygame.mouse.get_pos()

        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                game_board.tile_deck.scroll_north()
            elif event.key == pygame.K_RIGHT:
                game_board.tile_deck.scroll_east()
            elif event.key == pygame.K_DOWN:
                game_board.tile_deck.scroll_south()
            elif event.key == pygame.K_LEFT:
                game_board.tile_deck.scroll_west()

        if not player_dialog.is_open():

            if event.type == pygame.MOUSEBUTTONDOWN:
                if game_board.status == GameBoardEnum.END:
                    # Simon added 20190127
                    x, y = CardDeck.stack_topleft
                    mouse_x, mouse_y = mouse_pos
                    if (
                        x < mouse_x < x + game_board.start_img.get_width() and
                        y < mouse_y < y + game_board.start_img.get_height()
                    ):
                        game_board.status = GameBoardEnum.NEW_GAME

                    for player in game_board.player_deck.players:
                        if player.border_rect.collidepoint(mouse_pos):
                            game_board.tile_deck.scroll_player(player)

            # computer player needs no interaction
            current_player = game_board.player_deck.current_player
            isComputerTurn = (
                game_board.status != GameBoardEnum.END and
                current_player.is_computer
            )

            if not isComputerTurn:

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if game_board.status == GameBoardEnum.DECK:
                        if game_board.card_deck.collidepoint(mouse_pos):
                            game_board.pick_card()
                        else:
                            for player in game_board.player_deck.players:
                                if player.border_rect.collidepoint(mouse_pos):
                                    game_board.tile_deck.scroll_player(player)
                    elif game_board.status == GameBoardEnum.CARD_GRABBED:
                        if game_board.tile_deck.collidepoint(mouse_pos):
                            game_board.depositCard()
                        else:
                            game_board.restore_card()
                    elif game_board.status == GameBoardEnum.CARD_ALMOST_TILE:
                        # game_board.processButtons()
                        pass
                    elif game_board.status == GameBoardEnum.FIGURE_GRABBED:
                        if current_player.last_tile.rect.collidepoint(mouse_pos):
                            game_board.depositMiniPlayer()
                        else:
                            game_board.restoreMiniPlayer()
                    else:
                        pass

                    game_board.processButtons()

        app.event(event)

    #keys = pygame.key.get_pressed()

    #if keys[pygame.K_w]: # North
    #    pass
